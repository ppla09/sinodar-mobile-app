package com.ppl.sinodar.activities;

import com.ppl.sinodar.controllers.QuickDialController;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.WindowManager;

public class QuickDialActivity extends ActionBarActivity {
	
	public static final String INSTANCE_CODE = "instanceCode";
	
	private QuickDialController controller;
	private int instanceCode;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quick_dial);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		instanceCode = getIntent().getIntExtra(INSTANCE_CODE, -1);
		
		controller = new QuickDialController(QuickDialActivity.this);
		controller.handleMessage(QuickDialController.QUICK_DIAL, instanceCode);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.dispose();
	}
	
	@Override
	public void onRestart() {
		super.onRestart();
		controller.handleMessage(QuickDialController.VIEW_BIG_MENU, instanceCode);
	}
}
