package com.ppl.sinodar.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.ppl.sinodar.helper.LocationHelper;

public class LocationPickerActivity extends FragmentActivity implements OnMapLongClickListener, OnMapClickListener, OnMarkerDragListener {
	
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	
	public static final int NO_LOCATION = 99999999;
	
	private GoogleMap map;
	private Button chooseBtn, cancelBtn;
	private double iLat, iLong;
	private boolean hasPicked = false;
	private MarkerOptions userMarker;

	@Override
	protected void onCreate(Bundle saveInstance) {
		super.onCreate(saveInstance);
		setContentView(R.layout.activity_location_picker);
		
		map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.location_picker_map)).getMap();
		chooseBtn = (Button) findViewById(R.id.location_picker_choose_btn);
		cancelBtn = (Button) findViewById(R.id.location_picker_cancel_btn);
		
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		map.setOnMarkerDragListener(this);
		map.setOnMapLongClickListener(this);
		map.setOnMapClickListener(this);
		map.setMyLocationEnabled(true);

		Location location = LocationHelper.getCurrentLoc(this, 500);
		double uLat, uLong;
		if(location != null) {
			uLat = location.getLatitude();
			uLong = location.getLongitude();
		} else {
			uLat = -6.364511;		// TODO : ganti jadi sesuatu deh
			uLong = 106.828742;		// TODO : ganti jadi sesuatu deh
		}
		double camLat = uLat, camLong = uLong;

		// create user location marker
		userMarker = new MarkerOptions().position(new LatLng(uLat, uLong)).title("Your location").icon(BitmapDescriptorFactory
						.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
		map.addMarker(userMarker).showInfoWindow();
		
		// create previously chosen instance location marker (if any)
		Intent intent = getIntent();
		if(intent.hasExtra(LATITUDE) && intent.hasExtra(LONGITUDE)) {
			iLat = intent.getDoubleExtra(LATITUDE, NO_LOCATION);
			iLong = intent.getDoubleExtra(LONGITUDE, NO_LOCATION);
			
			map.addMarker(new MarkerOptions().position(new LatLng(iLat, iLong)).title("Instance location").icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_RED)).snippet("Hold to make new").draggable(true)).showInfoWindow();
			
			hasPicked = true;
			camLat = iLat; camLong= iLong;
		}
		
		// use map to move camera into position
		CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(camLat, camLong)).zoom(17.5F).bearing(300F).build();
		map.moveCamera(CameraUpdateFactory.newCameraPosition(INIT));
		
		chooseBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(hasPicked) finish();
					else Toast.makeText(LocationPickerActivity.this, R.string.location_not_picked, Toast.LENGTH_SHORT).show();
			}
		});

		cancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				hasPicked = false;
				map.clear();
				map.addMarker(userMarker);
			}
		});
	}

	@Override
	public void finish() {
		Intent intent = new Intent();
		
		if(hasPicked) {
			intent.putExtra(LATITUDE, iLat);
			intent.putExtra(LONGITUDE, iLong);
			setResult(RESULT_OK, intent);
		} else setResult(RESULT_CANCELED, intent);
		
		super.finish();
	}
	
	@Override
	public void onMarkerDrag(Marker arg0) {}

	@Override
	public void onMarkerDragEnd(Marker arg0) {
		LatLng dragPosition = arg0.getPosition();
		iLat = dragPosition.latitude;
		iLong = dragPosition.longitude;
	}

	@Override
	public void onMarkerDragStart(Marker arg0) {}

	@Override
	public void onMapClick(LatLng arg0) {
		map.animateCamera(CameraUpdateFactory.newLatLng(arg0));
	}

	@Override
	public void onMapLongClick(LatLng arg0) {
		// create new marker/replace old marker when user long clicks
		map.clear();
		map.addMarker(userMarker);
		map.addMarker(new MarkerOptions().position(arg0).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).draggable(true));
		hasPicked = true;
		
		iLong = arg0.longitude;
		iLat = arg0.latitude;
	}
}