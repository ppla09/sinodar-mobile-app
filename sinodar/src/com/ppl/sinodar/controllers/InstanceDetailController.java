package com.ppl.sinodar.controllers;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Intents;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.CommonDataKinds.Phone;

import com.ppl.sinodar.activities.EditInstanceActivity;
import com.ppl.sinodar.activities.InstanceDetailActivity;
import com.ppl.sinodar.activities.InstanceListActivity;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.daos.InstanceDao;
import com.ppl.sinodar.daos.SessionDao;
import com.ppl.sinodar.helper.ImageHelper;
import com.ppl.sinodar.models.UserSessionModel;
import com.ppl.sinodar.models.ViewedInstanceModel;

public class InstanceDetailController extends Controller {
	
	/* code for handling message */
	public static final int MESSAGE_LOAD_PICTURE = 1;
	public static final int MESSAGE_LOAD_MAP = 2;
	public static final int MESSAGE_SAVE_CONTACT = 3;
	public static final int MESSAGE_OVERWRITE_CONFIRMED = 4;
	public static final int VIEW_MAIN_MENU = 5;
	public static final int VIEW_MAP_ROAD = 6;
	public static final int VIEW_EDIT_FORM = 7;

	private InstanceDetailActivity view;
	private UserSessionModel session;
	private ViewedInstanceModel model;

	public InstanceDetailController(InstanceDetailActivity view, int instanceId) {
		this.view = view;
		session = new UserSessionModel();
		model = new ViewedInstanceModel();
		model.addListener(this.view);
				
		workerThread = new HandlerThread("InstanceDetailController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
		
		checkUserSession();
		getInstanceDetail(instanceId);
	}
	
	public UserSessionModel getSession() {
		return session;
	}

	@Override
	public boolean handleMessage(int what, Object data) {
		switch (what) {
			case MESSAGE_LOAD_PICTURE:
				loadInstancePicture();
				return true;
			
			case MESSAGE_LOAD_MAP:
				loadMapImage();
				return true;
				
			case MESSAGE_SAVE_CONTACT:
				tryToSaveContact();
				return true;
				
			case MESSAGE_OVERWRITE_CONFIRMED:
				saveContact(true);
				return true;
				
			case VIEW_MAP_ROAD:
				viewMapRoad();
				return true;
			
			case VIEW_EDIT_FORM:
				viewEditForm();
				return true;
		}
		return false;
	}
	
	private void checkUserSession() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (session) {
					UserSessionModel saved = SessionDao.loadUserSession(view);
					if (saved != null) {
						session.consume(saved);
						view.supportInvalidateOptionsMenu();
					}
				}
			}
		});
	}

	private void getInstanceDetail(final int instanceId) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				view.onLoading();
				ViewedInstanceModel acquired = null;

				try {
					acquired = InstanceDao.getViewedInstanceDetail(instanceId, view);
				} catch (Exception e) {
					Intent intent = new Intent(view, InstanceListActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent.putExtra(InstanceListActivity.SHOW_TOAST_UNSTABLE_INTERNET, true);
					
					view.startActivity(intent);
				}

				if (acquired == null) {
					Intent intent = new Intent(view, InstanceListActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent.putExtra(InstanceListActivity.SHOW_TOAST_DATABASE_ERROR, true);
					
					view.startActivity(intent);
				} else {
					view.onResumeDetail();
					model.consume(acquired);
				}
			}
		});
	}
	
	private void loadInstancePicture() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				view.onLoadingInstancePic();
				
				Bitmap image;
				String URL = model.getPictureURL();
				if(URL.length() > 0) image = ImageHelper.getImage(URL); 
					else image = ((BitmapDrawable) view.getResources().getDrawable(R.drawable.no_picture_available)).getBitmap();
				
		        view.onLoadedInstancePic(image);
			}
		});		
	}
	
	private void loadMapImage() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
		        int partScreen = ImageHelper.getDeviceWidth(view) * 6 / 7;
		        if (partScreen < 400) partScreen = 400;
		        
		        double latitude = Math.toDegrees(model.getLatitude());
		        double longitude = Math.toDegrees(model.getLongitude());
		        String url = "http://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude
		        		+ "&zoom=15&size=" + partScreen + "x" + partScreen + "&markers=color:red%7C" + latitude 
		        		+ "," + longitude + "&sensor=false&key=AIzaSyCYXhc_OOotrnbx5T-wNWUPak81c7f6tno";
		        
		        view.onLoadingInstanceMap();
		        Bitmap image = ImageHelper.getImage(url);
		        view.onLoadedInstanceMap(image);
			}
		});		
	}

	@SuppressLint("DefaultLocale")
	private void viewMapRoad() {
		String mapURL = String.format("http://maps.google.com/maps?daddr=%f,%f (%s)", Math.toDegrees(model.getLatitude()), Math.toDegrees(model.getLongitude()), model.getName());
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(mapURL));
		view.startActivity(intent);
	}
	
	private void viewEditForm() {
		Intent intent = new Intent(view, EditInstanceActivity.class);
		intent.putExtra(EditInstanceActivity.USER_NAME, session.getUserName());	
		intent.putExtra(EditInstanceActivity.INSTANCE_DATA, model);
		
		if(model.getPictureURL().length() > 0) {
			ImageHelper.saveTempImageToStorage(view.getInstancePic(), EditInstanceActivity.TEMP_IMAGE_FILENAME);
			intent.putExtra(EditInstanceActivity.INSTANCE_PIC_PATH, EditInstanceActivity.TEMP_IMAGE_FILENAME);
		}
		
		view.startActivity(intent);
	}

	public void tryToSaveContact() {
		Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(model.getMainPhoneNumber()));
		String[] mPhoneNumberProjection = { PhoneLookup._ID, PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
		Cursor cur = view.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
		
		boolean contactExists = false;
		try {
			if (cur.moveToFirst()) contactExists = true;
		} finally {
			if (cur != null) cur.close();
		}
		
		if(contactExists) view.createOverwriteDialog(); else saveContact(false); 
	}
	
	// source: http://developer.android.com/training/contacts-provider/modify-data.html
	private void saveContact(boolean overwrite) {
		if(overwrite) {
			deleteContact(view.context, model.getMainPhoneNumber(), model.getName());
		}
		
		Intent intentSave = new Intent(Intent.ACTION_INSERT);
		intentSave.setType(Contacts.CONTENT_TYPE);
		intentSave.putExtra("finishActivityOnSaveCompleted", true);
		
		// Insert data to intent
		intentSave.putExtra(Intents.Insert.NAME, model.getName());
		
		// Saving phone number
		intentSave.putExtra(Intents.Insert.PHONE, model.getMainPhoneNumber());
		intentSave.putExtra(Intents.Insert.PHONE_ISPRIMARY, Integer.valueOf(1));		
		intentSave.putExtra(Intents.Insert.PHONE_TYPE, Phone.TYPE_WORK);
		
		ArrayList<String> otherNum = model.getOtherPhoneNumber();
		if(otherNum.size() >= 1) {
			intentSave.putExtra(Intents.Insert.SECONDARY_PHONE, otherNum.get(0));
			intentSave.putExtra(Intents.Insert.SECONDARY_PHONE_TYPE, Phone.TYPE_OTHER);
		}
		
		if(otherNum.size() >= 2) {
			intentSave.putExtra(Intents.Insert.TERTIARY_PHONE, otherNum.get(1));
			intentSave.putExtra(Intents.Insert.TERTIARY_PHONE_TYPE, Phone.TYPE_OTHER);
		}
		
		view.startActivity(intentSave);
	}
	
	public static boolean deleteContact(Context ctx, String phone, String name) {
	    Uri contactUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
	    Cursor cur = ctx.getContentResolver().query(contactUri, null, null, null, null);
	    try {
	        if (cur.moveToFirst()) {
	            do {
	                if (cur.getString(cur.getColumnIndex(PhoneLookup.DISPLAY_NAME)).equalsIgnoreCase(name)) {
	                    String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
	                    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
	                    ctx.getContentResolver().delete(uri, null, null);
	                    return true;
	                }

	            } while (cur.moveToNext());
	        }

	    } catch (Exception e) {
	        System.out.println(e.getStackTrace());
	    }
	    return false;
	}
}
