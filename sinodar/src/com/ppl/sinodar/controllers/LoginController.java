package com.ppl.sinodar.controllers;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import android.widget.Toast;

import com.ppl.sinodar.activities.LoginActivity;
import com.ppl.sinodar.activities.MainMenuActivity;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.activities.RegisterActivity;
import com.ppl.sinodar.daos.SessionDao;
import com.ppl.sinodar.helper.ConnectionHelper;
import com.ppl.sinodar.models.UserSessionModel;

public class LoginController extends Controller {

	public static final int MESSAGE_USER_LOGIN = 1;
	public static final int VIEW_REGISTER_FORM = 2;

	private LoginActivity view;

	public LoginController(LoginActivity view) {
		this.view = view;

		workerThread = new HandlerThread("LoginController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean handleMessage(int what, Object data) {
		switch (what) {
		case MESSAGE_USER_LOGIN:
			login((ArrayList<String>) data);
			return true;

		case VIEW_REGISTER_FORM:
			viewRegisterForm();
			return true;
		}
		return false;
	}

	private void viewRegisterForm() {
		Intent intent = new Intent(view, RegisterActivity.class);
		view.startActivity(intent);
	}

	private boolean validateField(String username, String password) {
		view.resetFieldError();
		boolean valid = true;

		if (TextUtils.isEmpty(password)) {
			view.setPasswordFieldError(R.string.error_field_required);
			valid = false;
		}

		if (TextUtils.isEmpty(username)) {
			view.setUserNameFieldError(R.string.error_field_required);
			valid = false;
		}

		return valid;
	}

	private void login(final ArrayList<String> data) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				String username = data.get(0); // by convention, idx 0 -> username
				String password = data.get(1); // 				 idx 1 -> password

				if (!validateField(username, password)) return;

				if (!ConnectionHelper.isOnline()) {
					Toast.makeText(view, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
					return;
				}

				view.onLoading();
				UserSessionModel model;
				try {
					model = SessionDao.postLoginData(username, password);
				} catch (Exception e) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.unstable_internet_connection_login, Toast.LENGTH_SHORT).show();
					return;
				}

				if (model == null) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.wrong_combination, Toast.LENGTH_SHORT).show();
				} else {
					SessionDao.saveUserSession(model);

					Intent intent = new Intent(view, MainMenuActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					view.startActivity(intent);
				}
			}
		});
	}
}
