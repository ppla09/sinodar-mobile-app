package com.ppl.sinodar.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.ppl.sinodar.SinodarApp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionHelper {
	
	/* Timeout constant in millisecond */
	public static final int CONNECTION_TIMEOUT = 5000;
	public static final int SOCKET_TIMEOUT = 10000;
	
	public static boolean isOnline() {
	    ConnectivityManager connMgr = (ConnectivityManager) SinodarApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	    return (networkInfo != null && networkInfo.isConnected());
	}  
	
	public static DefaultHttpClient setHttpClient() {
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParameters, SOCKET_TIMEOUT);
		
		return new DefaultHttpClient(httpParameters);
	}
	
	public static String getExecuteResult(HttpClient httpClient, HttpPost httpPost) 
			throws UnknownHostException, ConnectTimeoutException, SocketTimeoutException {
		String ret = null;
		
		try { 
			HttpResponse response = httpClient.execute(httpPost);
			ret = inputStreamToString(response.getEntity().getContent()).toString();
		} catch (UnknownHostException uhe) {
			//Unable to determine host's IP address
			throw uhe;
		} catch (ConnectTimeoutException cte ){ 
			//Took too long to connect to remote host
			throw cte;
		} catch (SocketTimeoutException ste){ 
			//Remote host didn�t respond in time
			throw ste;
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	private static StringBuilder inputStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is)); // Wrap a BufferedReader around the InputStream
		
		// Read response until the end
		try {
			while ((line = rd.readLine()) != null) total.append(line); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	    return total; // Return full string
	}
}
