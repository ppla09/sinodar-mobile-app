package com.ppl.sinodar.models;

import java.io.Serializable;

public class InstanceModel extends SimpleObservable<InstanceModel> implements Serializable {
	
	private static final long serialVersionUID = 1L; /* required for Serializable */
	
	protected int id = -1;
	protected String name = "";
	protected int instanceType;
	protected double latitude;	// must be in radian!
	protected double longitude; 	// must be in radian!
	protected String mainPhoneNumber = "";

	public int getId() { return id; }
	public String getName() { return name; }
	public int getInstanceType() { return instanceType; }
	public double getLatitude() { return latitude; }
	public double getLongitude() { return longitude; }
	public String getMainPhoneNumber() { return mainPhoneNumber; }
	
	public void setId(int id) {
		this.id = id;
		notifyObservers(this);
	}

	public void setName(String name) {
		this.name = name;
		notifyObservers(this);
	}

	public void setInstanceType(int instanceType) {
		this.instanceType = instanceType;
		notifyObservers(this);
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
		notifyObservers(this);
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
		notifyObservers(this);
	}
	
	public void setMainPhoneNumber(String mainPhoneNumber) {
		this.mainPhoneNumber = mainPhoneNumber;
		notifyObservers(this);
	}

	@Override
	synchronized public InstanceModel clone() {
		InstanceModel model = new InstanceModel();
		model.setId(id);
		model.setName(name);
		model.setInstanceType(instanceType);
		model.setLatitude(latitude);
		model.setLongitude(longitude);
		model.setMainPhoneNumber(mainPhoneNumber);
		return model;
	}
	
	synchronized public void consume(InstanceModel model) {
		this.id = model.getId();
		this.name = model.getName();
		this.instanceType = model.getInstanceType();
		this.latitude = model.getLatitude();
		this.longitude = model.getLongitude();
		this.mainPhoneNumber = model.getMainPhoneNumber();
		notifyObservers(this);
	}
}
