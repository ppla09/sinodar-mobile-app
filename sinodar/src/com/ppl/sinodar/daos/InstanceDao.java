package com.ppl.sinodar.daos;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.location.Location;
import android.net.Uri;
import android.widget.Toast;

import com.ppl.sinodar.SinodarApp;
import com.ppl.sinodar.helper.ConnectionHelper;
import com.ppl.sinodar.helper.ImageHelper;
import com.ppl.sinodar.helper.LocationHelper;
import com.ppl.sinodar.models.InstanceModel;
import com.ppl.sinodar.models.ViewedInstanceModel;

@SuppressWarnings("deprecation")
public class InstanceDao {

	public ArrayList<InstanceModel> instance_list = new ArrayList<InstanceModel>();
	
	/* Web page and file name constant */
	private static final String DOMAIN = "http://mahasiswa.cs.ui.ac.id/~rifky.fakhrul/sinodar/web-service/";
	private static final String INSTANCE_DETAIL_PAGE = DOMAIN + "fd502b68fb03524713f0cf108a80a981.php";
	private static final String INSTANCE_LIST_PAGE 	 = DOMAIN + "5f31953a687cebaf2f438be9aaeab3ac.php";
	private static final String SUBMIT_INSTANCE_PAGE = DOMAIN + "5d79a84ecedb9e796c63bede68f47c5c.php";
	private static final String EDIT_INSTANCE_PAGE   = DOMAIN + "9db417a23478efdfb26dbc3c1bb64da9.php";
	private static final String LOCAL_CACHE_PAGE     = DOMAIN + "88a712e961cd867314bfc8b3e7c251e4.php";
	private static final String HOSPITAL_CACHE_FILE  	  = "hospital_cache.json";
	private static final String FIRE_STATION_CACHE_FILE   = "fire_station_cache.json";
	private static final String POLICE_STATION_CACHE_FILE = "police_station_cache.json";

	/* JSON field name constant */
	private static final String ID 			   = "id";
	private static final String USERNAME	   = "username";
	private static final String TYPE 	       = "type";
	private static final String NAME 		   = "name";
	private static final String ADDRESS 	   = "address";
	private static final String LATITUDE 	   = "latitude";
	private static final String LONGITUDE 	   = "longitude";
	private static final String DESCRIPTION    = "description";
	private static final String PICTURE_URL    = "pictureURL";
	private static final String DISTANCE	   = "distance";
	private static final String PRIMARY_NUM    = "primaryPhoneNum";
	private static final String OTHER_NUM 	   = "otherPhoneNum";
	private static final String HOSPITAL	   = "hospital";
	private static final String FIRE_STATION   = "fire station";
	private static final String POLICE_STATION = "police station";
	
	/* Required constant */
	private static final int EARTH_RADIUS = 6371; 	  // in km 
	private static final int MAX_RADIUS_LIST = 25;	  // in km
	private static final int MAX_RADIUS_DIAL = 10;	  // in km

	private static ViewedInstanceModel consumeJSON(String toJSON) throws JSONException {
		JSONObject json = new JSONObject(toJSON);
		ViewedInstanceModel ret = new ViewedInstanceModel();

		ret.setName(json.getString(NAME));
		ret.setAddress(json.getString(ADDRESS));
		ret.setLatitude(json.getDouble(LATITUDE));
		ret.setLongitude(json.getDouble(LONGITUDE));
		ret.setDescription(json.getString(DESCRIPTION));
		ret.setPictureURL(json.getString(PICTURE_URL));
		ret.setMainPhoneNumber(json.getString(PRIMARY_NUM));
		
		JSONArray jsonarray = json.getJSONArray(OTHER_NUM);
		ArrayList<String> otherNum = new ArrayList<String>();
		for (int i = 0; i < jsonarray.length(); i++) otherNum.add(jsonarray.getString(i));
		ret.setOtherPhoneNumber(otherNum);
		return ret;
	}
	
	private static ArrayList<ViewedInstanceModel> consumeJSONArrayList(String toJSON) throws JSONException {
		JSONArray jsonarray = new JSONArray(toJSON);
		ArrayList<ViewedInstanceModel> ret = new ArrayList<ViewedInstanceModel>();
		
		for(int i = 0; i < jsonarray.length(); i++) {
			JSONObject json = jsonarray.getJSONObject(i);
			ViewedInstanceModel model = new ViewedInstanceModel();
			
			model.setId(json.getInt(ID));
			model.setName(json.getString(NAME));
			model.setDistance(json.getDouble(DISTANCE));
			model.setPictureURL(json.getString(PICTURE_URL));
			ret.add(model);
		}

		return ret;
	}
	
	private static ArrayList<InstanceModel> consumeJSONArrayCalled(String toJSON) throws JSONException {
		JSONArray jsonarray = new JSONArray(toJSON);
		ArrayList<InstanceModel> ret = new ArrayList<InstanceModel>();
		
		for(int i = 0; i < jsonarray.length(); i++) {
			JSONObject json = jsonarray.getJSONObject(i);
			InstanceModel model = new InstanceModel();
			
			model.setName(json.getString(NAME));
			model.setLatitude(json.getDouble(LATITUDE));
			model.setLongitude(json.getDouble(LONGITUDE));
			model.setMainPhoneNumber(json.getString(PRIMARY_NUM));
			ret.add(model);
		}

		return ret;
	}
	
	/* taken from http://www.movable-type.co.uk/scripts/latlong.html */
	public static double calculateGCD(double lat1, double lon1, double lat2, double lon2) {
		double dLat = lat2 - lat1;
		double dLon = lon2 - lon1;
		
		double a = Math.pow(Math.sin(dLat / 2), 2) + Math.cos(lon1) * Math.cos(lon2) * Math.pow(Math.sin(dLon / 2), 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return EARTH_RADIUS * c;
	}

	public static ViewedInstanceModel getViewedInstanceDetail(int instanceId, Activity view) throws Exception {
		Location now_loc = LocationHelper.getCurrentLoc(view, 500);
		double latitude = Math.toRadians(now_loc.getLatitude());
		double longitude = Math.toRadians(now_loc.getLongitude());
		
		HttpClient httpClient = ConnectionHelper.setHttpClient();
		HttpPost httpPost = new HttpPost(INSTANCE_DETAIL_PAGE);
		
		try {
			ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("id", Integer.toString(instanceId)));
			nameValuePairs.add(new BasicNameValuePair("latitude", Double.toString(latitude)));
			nameValuePairs.add(new BasicNameValuePair("longitude", Double.toString(longitude)));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			String toJSON = ConnectionHelper.getExecuteResult(httpClient, httpPost);
			
			if(!toJSON.equals("instance not found")) {
				ViewedInstanceModel ret = consumeJSON(toJSON);
				ret.setId(instanceId);
				ret.setDistance(calculateGCD(ret.getLatitude(), ret.getLongitude(), latitude, longitude));
				return ret;
			}
		} catch (UnknownHostException | ConnectTimeoutException | SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static boolean postSubmitInstance(Context ctx, String username, String type, String name, String addr, String desc, double lat, double lon, int chosenMain, ArrayList<String> phoneNum, Uri picUri) throws Exception {
		HttpClient httpClient = ConnectionHelper.setHttpClient();
		HttpPost httpPost = new HttpPost(SUBMIT_INSTANCE_PAGE);
		
		boolean success = false;
		try {
			JSONObject toSend = new JSONObject();
			JSONArray otherNum = new JSONArray();
		
			toSend.put(USERNAME, username);
			toSend.put(TYPE, type);
			toSend.put(NAME, name);
			toSend.put(ADDRESS, addr);
			toSend.put(DESCRIPTION, desc);
			toSend.put(LATITUDE, Math.toRadians(lat));
			toSend.put(LONGITUDE, Math.toRadians(lon));
			toSend.put(PRIMARY_NUM, phoneNum.get(chosenMain));
			
			for(int i = 0; i < phoneNum.size(); i++) if(i != chosenMain && !phoneNum.get(i).isEmpty()) otherNum.put(phoneNum.get(i));
			toSend.put(OTHER_NUM, otherNum);
			
			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			entity.addPart("jsondata", new StringBody(toSend.toString()));
			if(picUri != null) {
				InputStream in = ImageHelper.compressImageFromUri(ctx, picUri, CompressFormat.JPEG, 70);
				entity.addPart("picture", new InputStreamBody(in, "image/jpeg", name + ".jpg"));
			}

			httpPost.setEntity(entity);
			String result = ConnectionHelper.getExecuteResult(httpClient, httpPost);
			
			success = result.equals("submit success");
		} catch (UnknownHostException | ConnectTimeoutException | SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return success;
	}
	
	public static boolean postEditInstance(Context ctx, int id, String username, String picName, String name, String addr, String desc, Double lat, Double lon, Integer chosenMain, int mainNum, ArrayList<String> phoneNum, Uri picUri) throws Exception {
		HttpClient httpClient = ConnectionHelper.setHttpClient();
		HttpPost httpPost = new HttpPost(EDIT_INSTANCE_PAGE);
		
		boolean success = false;
		try {
			JSONObject toSend = new JSONObject();
			JSONArray otherNum = new JSONArray();
		
			toSend.put(ID, id);
			toSend.put(USERNAME, username);
			if(name != null) toSend.put(NAME, name);
			if(addr != null) toSend.put(ADDRESS, addr);
			if(desc != null) toSend.put(DESCRIPTION, desc);
			if(lat  != null) toSend.put(LATITUDE, Math.toRadians(lat));
			if(lon  != null) toSend.put(LONGITUDE, Math.toRadians(lon));
			if(chosenMain != null) toSend.put(PRIMARY_NUM, phoneNum.get(chosenMain));
			
			if(phoneNum != null) {
				for(int i = 0; i < phoneNum.size(); i++)
					if(i != mainNum && !phoneNum.get(i).isEmpty()) otherNum.put(phoneNum.get(i));
				toSend.put(OTHER_NUM, otherNum);
			}
		
			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			entity.addPart("jsondata", new StringBody(toSend.toString()));
			if(picUri != null) {
				InputStream in = ImageHelper.compressImageFromUri(ctx, picUri, CompressFormat.JPEG, 70);
				entity.addPart("picture", new InputStreamBody(in, "image/jpeg", picName + ".jpg"));
			}

			httpPost.setEntity(entity);
			String result = ConnectionHelper.getExecuteResult(httpClient, httpPost);
			
			success = result.equals("edit success");
		} catch (UnknownHostException | ConnectTimeoutException | SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return success;
	}
	
	public static ArrayList<ViewedInstanceModel> getViewedInstanceList(int instanceCode, Activity view) throws Exception {
		Location now_loc = LocationHelper.getCurrentLoc(view, 500);
		double latitude = Math.toRadians(now_loc.getLatitude());
		double longitude = Math.toRadians(now_loc.getLongitude());
		
		HttpClient httpClient = ConnectionHelper.setHttpClient();
		HttpPost httpPost = new HttpPost(INSTANCE_LIST_PAGE);
		
		String instanceType = null;
		switch(instanceCode) {
			case SinodarApp.HOSPITAL: instanceType = "hospital"; break;
			case SinodarApp.FIRE_STATION: instanceType = "fire station"; break;
			case SinodarApp.POLICE_STATION: instanceType = "police station"; break;
		}
		
		try {
			ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(5);
			nameValuePairs.add(new BasicNameValuePair("instancetype", instanceType));
			nameValuePairs.add(new BasicNameValuePair("latitude"    , Double.toString(latitude)));
			nameValuePairs.add(new BasicNameValuePair("longitude"   , Double.toString(longitude)));
			nameValuePairs.add(new BasicNameValuePair("earthradius" , Double.toString(EARTH_RADIUS)));
			nameValuePairs.add(new BasicNameValuePair("maxradius"   , Double.toString(MAX_RADIUS_LIST)));
			
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			String toJSON = ConnectionHelper.getExecuteResult(httpClient,httpPost);
			
			return consumeJSONArrayList(toJSON);
		} catch (UnknownHostException | ConnectTimeoutException | SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static boolean updateLocalCache(Activity view) throws Exception {
		Location now_loc = LocationHelper.getCurrentLoc(view, 500);
		double latitude = Math.toRadians(now_loc.getLatitude());
		double longitude = Math.toRadians(now_loc.getLongitude());
		
		HttpClient httpClient = ConnectionHelper.setHttpClient();
		HttpPost httpPost = new HttpPost(LOCAL_CACHE_PAGE);
		
		boolean success = false;
		try {
			ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(4);
			nameValuePairs.add(new BasicNameValuePair("latitude"    , Double.toString(latitude)));
			nameValuePairs.add(new BasicNameValuePair("longitude"   , Double.toString(longitude)));
			nameValuePairs.add(new BasicNameValuePair("earthradius" , Double.toString(EARTH_RADIUS)));
			nameValuePairs.add(new BasicNameValuePair("maxradius"   , Double.toString(MAX_RADIUS_DIAL)));
			
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			String toJSON = ConnectionHelper.getExecuteResult(httpClient,httpPost);
			System.out.println(toJSON);
			
			if(!toJSON.equals("location error")) {

				JSONObject json = new JSONObject(toJSON);
				JSONArray hospital = json.getJSONArray(HOSPITAL);
				JSONArray firestation = json.getJSONArray(FIRE_STATION);
				JSONArray polstation = json.getJSONArray(POLICE_STATION);
				
				saveCacheFile(hospital, HOSPITAL_CACHE_FILE);
				saveCacheFile(firestation, FIRE_STATION_CACHE_FILE);
				saveCacheFile(polstation, POLICE_STATION_CACHE_FILE);
				
				success = true;
			}
		} catch (UnknownHostException | ConnectTimeoutException | SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return success;
	}
	
	private static void saveCacheFile(JSONArray toSave, String filename) {
		try {
			FileOutputStream outputStream = SinodarApp.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
			outputStream.write(toSave.toString().getBytes());
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<InstanceModel> getCalledInstance(int instanceCode, Activity view) throws Exception {
		ArrayList<InstanceModel> model = null;
		
		String filename = null;
		switch(instanceCode) {
			case(SinodarApp.HOSPITAL) : filename = HOSPITAL_CACHE_FILE; break;
			case(SinodarApp.FIRE_STATION) : filename = FIRE_STATION_CACHE_FILE; break;
			case(SinodarApp.POLICE_STATION) : filename = POLICE_STATION_CACHE_FILE; break;
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(view.openFileInput(filename)));
			
			model = consumeJSONArrayCalled(reader.readLine());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return model;	// null if no file before, handle this in QuickDial
	}	
}
