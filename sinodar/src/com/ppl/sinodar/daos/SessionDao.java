package com.ppl.sinodar.daos;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.ppl.sinodar.SinodarApp;
import com.ppl.sinodar.helper.ConnectionHelper;
import com.ppl.sinodar.models.UserSessionModel;

public class SessionDao {

	/* Web page and file name constant */
	private static final String DOMAIN = "http://mahasiswa.cs.ui.ac.id/~rifky.fakhrul/sinodar/web-service/";
	private static final String LOGIN_PAGE    = DOMAIN + "d56b699830e77ba53855679cb1d252da.php";
	private static final String REGISTER_PAGE = DOMAIN + "9de4a97425678c5b1288aa70c1669a64.php";
	private static final String SESSION_FILE = "session.json";

	/* JSON field name constant */
	private static final String USER_NAME 		= "userName";
	private static final String USER_FIRST_NAME = "userFirstName";

	private static UserSessionModel consumeJSON(String toJSON) throws JSONException {
		JSONObject json = new JSONObject(toJSON);
		UserSessionModel ret = new UserSessionModel();

		ret.setIsLogged(true);
		ret.setUserName(json.getString(USER_NAME));
		ret.setUserFirstName(json.getString(USER_FIRST_NAME));

		return ret;
	}

	public static UserSessionModel postLoginData(String userName, String password) throws Exception {
		HttpClient httpClient = ConnectionHelper.setHttpClient();
		HttpPost httpPost = new HttpPost(LOGIN_PAGE);

		try {
			ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("username", userName));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			String toJSON = ConnectionHelper.getExecuteResult(httpClient, httpPost);

			if (!toJSON.equals("login failed")) return consumeJSON(toJSON);
		} catch (UnknownHostException | ConnectTimeoutException | SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static UserSessionModel postRegisterData(String userName, String password, String email, String userFirstName, String userLastName) throws Exception {
		HttpClient httpClient = ConnectionHelper.setHttpClient();
		HttpPost httpPost = new HttpPost(REGISTER_PAGE);

		try {
			ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("username", userName));
			nameValuePairs.add(new BasicNameValuePair("password", password));
			nameValuePairs.add(new BasicNameValuePair("email", email));
			nameValuePairs.add(new BasicNameValuePair("userfirstname", userFirstName));
			if (userLastName != null) nameValuePairs.add(new BasicNameValuePair("userlastname", userLastName));

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			String res = ConnectionHelper.getExecuteResult(httpClient, httpPost);

			if (!res.equals("username is taken")) {
				UserSessionModel ret = new UserSessionModel();

				ret.setIsLogged(true);
				ret.setUserName(userName);
				ret.setUserFirstName(userFirstName);

				return ret;
			}
		} catch (UnknownHostException | ConnectTimeoutException | SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void saveUserSession(UserSessionModel model) {
		try {
			JSONObject json = new JSONObject();

			json.put(USER_NAME, model.getUserName());
			json.put(USER_FIRST_NAME, model.getUserFirstName());

			FileOutputStream outputStream = SinodarApp.getContext().openFileOutput(SESSION_FILE, Context.MODE_PRIVATE);
			outputStream.write(json.toString().getBytes());
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static UserSessionModel loadUserSession(Context view) {
		UserSessionModel model = null;

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(view.openFileInput(SESSION_FILE)));
			model = consumeJSON(reader.readLine());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return model;
	}

	public static void deleteUserSession(Context view) {
		view.deleteFile(SESSION_FILE);
	}
}
