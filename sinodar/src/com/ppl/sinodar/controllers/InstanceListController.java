package com.ppl.sinodar.controllers;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;

import com.ppl.sinodar.activities.BigMenuActivity;
import com.ppl.sinodar.activities.InstanceDetailActivity;
import com.ppl.sinodar.activities.InstanceListActivity;
import com.ppl.sinodar.daos.InstanceDao;
import com.ppl.sinodar.models.ViewedInstanceModel;

public class InstanceListController extends Controller {
	
	/* code for handling message */
	public static final int POPULATE_LIST = 1;
	public static final int GET_INSTANCE_DETAIL = 2;
	public static final int MESSAGE_MODEL_UPDATED = 3;
	
	private InstanceListActivity view;
	private ArrayList<ViewedInstanceModel> model;

	public InstanceListController(InstanceListActivity view) {
		this.view = view;
		model = new ArrayList<ViewedInstanceModel>();
		
		workerThread = new HandlerThread("InstanceListController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
	}
	
	public ArrayList<ViewedInstanceModel> getModel() { return model; };
	
	@Override
	public boolean handleMessage(int what, Object data) {
		switch (what) {
			case POPULATE_LIST:
				populateList((int) data);
				return true;
			
			case GET_INSTANCE_DETAIL:
				viewInstanceDetail((int) data);
				return true;
		}
		return false;
	}
	
	private void viewInstanceDetail(int instanceId) {
		Intent intent = new Intent(view, InstanceDetailActivity.class);
		intent.putExtra(InstanceDetailActivity.INSTANCE_ID, instanceId);
		
		view.startActivity(intent);
	}
	
	public void populateList(final int instanceCode) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				view.onLoading();
				ArrayList<ViewedInstanceModel> acquired = null;

				try {
					acquired = InstanceDao.getViewedInstanceList(instanceCode, view);
				} catch (Exception e) {
					Intent intent = new Intent(view, BigMenuActivity.class);
					intent.putExtra(BigMenuActivity.SHOW_TOAST_UNSTABLE_INTERNET, true);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					
					view.startActivity(intent);
				}
				
				view.onResumeList();
				synchronized(model) {
					model.clear();
					for(ViewedInstanceModel data : acquired) model.add(data);
					notifyOutboxHandlers(MESSAGE_MODEL_UPDATED, 0, 0, null);
				}
			}
		});
	}
}
