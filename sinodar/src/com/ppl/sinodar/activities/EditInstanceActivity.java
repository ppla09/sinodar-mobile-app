package com.ppl.sinodar.activities;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import com.ppl.sinodar.animations.Gate;
import com.ppl.sinodar.controllers.EditInstanceController;
import com.ppl.sinodar.models.ViewedInstanceModel;

public class EditInstanceActivity extends ActionBarActivity {

	public static final String USER_NAME = "username";
	public static final String INSTANCE_DATA = "serializedData";
	public static final String INSTANCE_PIC_PATH = "instancePicPath";
	public static final String TEMP_IMAGE_FILENAME = "temp.jpg";
	private static final String GATE_STATUS = "thisIsGate";

	private final String FIELD_HOLDER = "fieldHolder";
	private final String RADIO_ON_INDEX = "radioIdx";
	private final String PHONE_FOCUS_INDEX = "phoneIdx";
	private final String CHOSEN_IMAGE_URI = "chosenUri";
	private final String CHOSEN_INSTANCE_LATITUDE = "latitude";
	private final String CHOSEN_INSTANCE_LONGITUDE = "longitude";
	
	private EditInstanceController controller;

	private int x2, x3;
	private boolean isOpened, isBackPressed;
	
	private LinearLayout fieldHolder;
	private RadioGroup mainPhoneRadioGroup;
	private EditText instanceName, instanceAddr, instanceDesc;
	private ImageView instancePic, instanceMap;
	private Button submitBtn;
	
	private View instanceMapProgress, editProgress;
	private ScrollView editForm; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_instance);
		
		fieldHolder = (LinearLayout) findViewById(R.id.edit_field_holder);
		mainPhoneRadioGroup = (RadioGroup) findViewById(R.id.edit_main_phone_radiogroup);
		instanceName = (EditText) findViewById(R.id.edit_instance_name);
		instanceAddr = (EditText) findViewById(R.id.edit_instance_address);
		instanceDesc = (EditText) findViewById(R.id.edit_instance_description);
		instancePic = (ImageView) findViewById(R.id.edit_instance_picture);
		instanceMap = (ImageView) findViewById(R.id.edit_instance_map);
		submitBtn = (Button) findViewById(R.id.edit_submit_btn);
		
		instanceMapProgress = findViewById(R.id.edit_instance_map_progress);
		editProgress = findViewById(R.id.loading);
		editForm = (ScrollView) findViewById(R.id.instance_list_scroll_view);
		
		addNumberField(true);
		ViewedInstanceModel data = (ViewedInstanceModel) getIntent().getSerializableExtra(INSTANCE_DATA);
		fillDefaultData(data);
		
		controller = new EditInstanceController(this, getIntent().getStringExtra(USER_NAME), data);
		controller.handleMessage(EditInstanceController.LOAD_INITIAL_PICTURE, getIntent().getStringExtra(INSTANCE_PIC_PATH));
		controller.handleMessage(EditInstanceController.LOAD_INITIAL_MAP);
		
		instancePic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				controller.handleMessage(EditInstanceController.MESSAGE_CHOOSE_PICTURE);
			}
		});
		
		instanceMap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				controller.handleMessage(EditInstanceController.MESSAGE_SET_INSTANCE_LOCATION);
			}
		});
		
		submitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				resetFieldError();
				
				/* record entered phone number and chosen main number */
				int chosenMainNumber = -1;
				ArrayList<String> phoneNumber = new ArrayList<String>();
				for(int i = 0, count = fieldHolder.getChildCount(); i < count; i++) {
					phoneNumber.add(((EditText) fieldHolder.getChildAt(i)).getText().toString());
					if(((RadioButton) mainPhoneRadioGroup.getChildAt(i)).isChecked()) chosenMainNumber = i;
				}
				
				/* record all data */
				ArrayList<Object> data = new ArrayList<Object>();
				data.add(instanceName.getText().toString());					// instance name -> idx 0
				data.add(instanceAddr.getText().toString());					// instance addr -> idx 1
				data.add(phoneNumber);											// all phone number -> idx 2
				data.add(chosenMainNumber);										// chosen main number -> idx 3
				data.add(instanceDesc.getText().toString());					// instance desc -> idx 4
				
				controller.handleMessage(EditInstanceController.MESSAGE_SUBMIT_DATA, data); // remaining data is in controller
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		Gate.initXGate(x2, x3);
		Gate.initGate(EditInstanceActivity.this, isOpened);
		if (isOpened == false) {
			findViewById(R.id.loading).setVisibility(View.GONE);
			findViewById(R.id.loading2).setVisibility(View.VISIBLE);
			Gate.changeLoadScreen(findViewById(R.id.loading2));
			Gate.openGate(this,(ImageView)findViewById(R.id.gate_left2),(ImageView)findViewById(R.id.gate_right2),(RelativeLayout)findViewById(R.id.middle_loading2));
			setGateStatus(true);
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isBackPressed == false) {
			findViewById(R.id.loading).setVisibility(View.GONE);
			findViewById(R.id.loading2).setVisibility(View.VISIBLE);
			Gate.changeLoadScreen(findViewById(R.id.loading2));
			Gate.crossfadeAkhir();
			Gate.initCloseGate(this);
			Gate.getLastMove().setAnimationListener(new AnimationListener(){
	            public void onAnimationStart(Animation a){}
	            public void onAnimationRepeat(Animation a){}
	            public void onAnimationEnd(Animation a){
	            	EditInstanceActivity.super.onBackPressed();
	            }
	        });
			Gate.startCloseGate((ImageView)findViewById(R.id.gate_left2),(ImageView)findViewById(R.id.gate_right2),(RelativeLayout)findViewById(R.id.middle_loading2));
			isBackPressed = true;
		}
	}

	@Override
	public void finish() {
		deleteFile(TEMP_IMAGE_FILENAME);
		super.finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.dispose();
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(GATE_STATUS, isOpened);
		
		ArrayList<String> enteredPhoneNum = new ArrayList<String>();
		for(int i = 0, count = fieldHolder.getChildCount(); i < count; i++) {
			EditText now = (EditText) fieldHolder.getChildAt(i);
			
			if(i + 1 < count || now.getText().toString().length() > 0) enteredPhoneNum.add(now.getText().toString());
			
			if(now.isFocused()) savedInstanceState.putInt(PHONE_FOCUS_INDEX, i);
			if(((RadioButton) mainPhoneRadioGroup.getChildAt(i)).isChecked()) savedInstanceState.putInt(RADIO_ON_INDEX, i);
		}
		
		savedInstanceState.putStringArrayList(FIELD_HOLDER, enteredPhoneNum);
		
		Uri chosenImageUri = controller.getChosenImageUri();
		if(chosenImageUri != null) savedInstanceState.putParcelable(CHOSEN_IMAGE_URI, chosenImageUri);
		
		if(controller.getHasInstanceLocationMoved()) {
			savedInstanceState.putDouble(CHOSEN_INSTANCE_LATITUDE, controller.getChosenInstanceLatitude());
			savedInstanceState.putDouble(CHOSEN_INSTANCE_LONGITUDE, controller.getChosenInstanceLongitude());
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.getBoolean(GATE_STATUS)) {
			setGateStatus(true);
		} else {
			setGateStatus(false);
		}
		
		ArrayList<String> enteredPhoneNum = savedInstanceState.getStringArrayList(FIELD_HOLDER);
		
		for(int i = 0; i < enteredPhoneNum.size(); i++) {
			EditText now = (EditText) fieldHolder.getChildAt(i);
			now.setText(enteredPhoneNum.get(i));
		}
		
		int toggledIdx = savedInstanceState.getInt(RADIO_ON_INDEX, -1);
		if(toggledIdx != -1) ((RadioButton) mainPhoneRadioGroup.getChildAt(toggledIdx)).toggle();
		
		int focusIdx = savedInstanceState.getInt(PHONE_FOCUS_INDEX, -1);
		if(focusIdx != -1) ((EditText) fieldHolder.getChildAt(focusIdx)).requestFocus();
		
		Uri chosenImageUri = savedInstanceState.getParcelable(CHOSEN_IMAGE_URI);
		if(chosenImageUri != null) controller.handleMessage(EditInstanceController.MESSAGE_HANDLE_PICTURE, chosenImageUri);
		
		double latitude = savedInstanceState.getDouble(CHOSEN_INSTANCE_LATITUDE, LocationPickerActivity.NO_LOCATION);
		double longitude = savedInstanceState.getDouble(CHOSEN_INSTANCE_LONGITUDE, LocationPickerActivity.NO_LOCATION);
		if(latitude != LocationPickerActivity.NO_LOCATION && longitude != LocationPickerActivity.NO_LOCATION) {
			ArrayList<Double> location = new ArrayList<Double>();
			location.add(latitude); location.add(longitude);
			controller.handleMessage(EditInstanceController.MESSAGE_HANDLE_LOCATION_PICKED, location);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == EditInstanceController.MESSAGE_CHOOSE_PICTURE && resultCode == Activity.RESULT_OK) {
			controller.handleMessage(EditInstanceController.MESSAGE_HANDLE_PICTURE, data.getData());
		} else if(requestCode == EditInstanceController.MESSAGE_SET_INSTANCE_LOCATION && resultCode == Activity.RESULT_OK) {
			ArrayList<Double> location = new ArrayList<Double>();
			location.add(data.getDoubleExtra(LocationPickerActivity.LATITUDE, LocationPickerActivity.NO_LOCATION));
			location.add(data.getDoubleExtra(LocationPickerActivity.LONGITUDE, LocationPickerActivity.NO_LOCATION));
			
			controller.handleMessage(EditInstanceController.MESSAGE_HANDLE_LOCATION_PICKED, location);
		}
	}
	
	public void onLoadingInstanceMap() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instanceMap.setVisibility(View.GONE);
				instanceMapProgress.setVisibility(View.VISIBLE);
			}
		});
	}
	
	public void onLoadedInstanceMap(final Bitmap image) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instanceMap.setVisibility(View.VISIBLE);
				instanceMapProgress.setVisibility(View.GONE);
				
				instanceMap.setImageBitmap(image);
			}
		});
	}
	
	public void onLoading() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.loading2).setVisibility(View.GONE);
				findViewById(R.id.loading).setVisibility(View.VISIBLE);
				changeAlphaLoading(findViewById(R.id.logging_in), 0.0f, 1.0f);
				findViewById(R.id.list_progress_bar).setVisibility(View.VISIBLE);
				Gate.changeLoadScreen(findViewById(R.id.loading));
				Gate.crossfadeAkhir();
				Gate.initCloseGate(EditInstanceActivity.this);
				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			}
		});
	}
	
	public void onRestoreForm() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.loading2).setVisibility(View.GONE);
				findViewById(R.id.loading).setVisibility(View.VISIBLE);
				changeAlphaLoading(findViewById(R.id.logging_in), 1.0f, 0.0f);
				findViewById(R.id.list_progress_bar).setVisibility(View.GONE);
				Gate.initXGate(x2, x3);
				Gate.initGate(EditInstanceActivity.this, isOpened);
				Gate.changeLoadScreen(findViewById(R.id.loading));
				Gate.openGate(EditInstanceActivity.this,(ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				setGateStatus(true);
			}
		});
	}
	
	private void fillDefaultData(final ViewedInstanceModel data) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instanceName.setText(data.getName());
				instanceAddr.setText(data.getAddress());
				instanceDesc.setText(data.getDescription());
				
				((EditText) fieldHolder.getChildAt(0)).setText(data.getMainPhoneNumber());
				((RadioButton) mainPhoneRadioGroup.getChildAt(0)).setChecked(true);
				
				ArrayList<String> otherNum = data.getOtherPhoneNumber();
				for(int i = 0; i < otherNum.size(); i++) ((EditText) fieldHolder.getChildAt(i + 1)).setText(otherNum.get(i));
			}
		});
	}
	
	private void addNumberField(final boolean firstField) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				EditText toAddText = new EditText(EditInstanceActivity.this);
				toAddText.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				toAddText.setInputType(InputType.TYPE_CLASS_NUMBER);
				toAddText.setMaxLines(1);
				if(firstField) toAddText.setHint(R.string.submit_prompt_phonenum);
					else toAddText.setHint(R.string.submit_prompt_phonenum_other);
				
				final int idx = fieldHolder.getChildCount();
				toAddText.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {}

					@Override
					public void afterTextChanged(Editable s) {
						if(s.toString().length() >= 0 && idx == fieldHolder.getChildCount() - 1) addNumberField(false);
					}
					
				});
				
				fieldHolder.addView(toAddText);
				
				RadioButton toAddRadio = new RadioButton(EditInstanceActivity.this);
				toAddRadio.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				mainPhoneRadioGroup.addView(toAddRadio);
			}
		});
	}
	
	public void setInstancePicture(final Bitmap bitmap) {
		runOnUiThread(new Runnable() {
			public void run() {
				instancePic.setImageBitmap(bitmap);
			}
		});
	}
	
	private void resetFieldError() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instanceName.setError(null);
				instanceAddr.setError(null);
				instanceDesc.setError(null);
				
				for(int i = 0, count = fieldHolder.getChildCount(); i < count; i++)
					((EditText) fieldHolder.getChildAt(i)).setError(null);
			}
		});
	}
	
	public void setInstanceNameFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instanceName.setError(getString(errorId));
				editForm.scrollTo(0, instanceName.getTop());
				instanceName.requestFocus();
			}
		});
	}
	
	public void setInstanceAddrFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instanceAddr.setError(getString(errorId));
				editForm.scrollTo(0, instanceAddr.getTop());
				instanceAddr.requestFocus();
			}
		});
	}
	
	public void setInstanceDescFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				instanceDesc.setError(getString(errorId));
				editForm.scrollTo(0, instanceDesc.getTop());
				instanceDesc.requestFocus();
			}
		});
	}
	
	public void setPhoneNumberFieldError(final int idx, final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				EditText field = (EditText) fieldHolder.getChildAt(idx);
				field.setError(getString(errorId));
				editForm.scrollTo(0, fieldHolder.getChildAt(idx).getTop());
				field.requestFocus();
			}
		});
	}

	private void setGateStatus(boolean is) {
		isOpened = is;
	}
	
	public static void changeAlphaLoading(View v, float from, float to) {
    	AlphaAnimation animateAlphaChange = new AlphaAnimation(from,to);
    	animateAlphaChange.setDuration(0);
    	animateAlphaChange.setFillAfter(true);
    	v.startAnimation(animateAlphaChange);
    }
}
