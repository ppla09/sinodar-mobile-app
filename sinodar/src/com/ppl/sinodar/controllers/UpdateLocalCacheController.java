package com.ppl.sinodar.controllers;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;

import com.ppl.sinodar.activities.MainMenuActivity;
import com.ppl.sinodar.activities.UpdateLocalCacheActivity;
import com.ppl.sinodar.daos.InstanceDao;

public class UpdateLocalCacheController extends Controller {

	public static final int UPDATE_LOCAL_CACHE = 1;
	
	private UpdateLocalCacheActivity view;
	
	public UpdateLocalCacheController(UpdateLocalCacheActivity view) {
		this.view = view;

		workerThread = new HandlerThread("UpdateLocalCacheController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
	}
	
	@Override
	public boolean handleMessage(int what, Object data) {
		switch(what) {
			case UPDATE_LOCAL_CACHE:
				updateCache();
				return true;
		}
		return false;
	}

	private void updateCache() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(view, MainMenuActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				
				try{
					InstanceDao.updateLocalCache(view);
				} catch (Exception e) {
					intent.putExtra(MainMenuActivity.SHOW_TOAST_UNSTABLE_INTERNET_CACHE, true);
					view.startActivity(intent);
					return;
				}
				
				intent.putExtra(MainMenuActivity.SHOW_TOAST_SUCCESS_CACHE, true);
				view.startActivity(intent);
			}
		});
	}
}
