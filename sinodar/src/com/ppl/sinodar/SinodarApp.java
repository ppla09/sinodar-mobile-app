package com.ppl.sinodar;

import android.app.Application;
import android.content.Context;

public class SinodarApp extends Application {

	/* code for instance type */
	public static final int HOSPITAL = 1;
	public static final int FIRE_STATION = 2;
	public static final int POLICE_STATION = 3;
	
	private static SinodarApp instance;
	
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}
	
	public static Context getContext() {
		return instance.getApplicationContext();
	}
}
