package com.ppl.sinodar.controllers;

import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ppl.sinodar.activities.HelpActivity;
import com.ppl.sinodar.activities.R;

public class HelpController extends Controller {

	/* code for message handling */
	public static final int QUICK_DIAL = 1;
	public static final int VIEW_DATA_LIST = 2;
	public static final int VIEW_DETAIL = 3;
	public static final int SAVE_CONTACT = 4;
	public static final int ACCOUNT = 5;
	public static final int CONTRIBUTE_INFORMATION = 6;
	public static final int UPDATE_DATA = 7;
	public static final int BACK = 8;
	public static final int NEXT = 9;
	public static final int BACK_TO_MENU = 10;
	public static final int NEXT_TO_STEPS = 11;
	private String[] help_header, help_detail;
	private Integer[] quick_dial_img, view_data_list_img, view_detail_img, save_contact_img, account_img, contribute_img, update_img;
	private Integer[][] content_package;
	private int whichPage, whichMenu, whichSteps;
	private boolean isLastImg;
	private TextView detail_header;
	private TextView detail_help;
	private ImageView slideShow;
	private RelativeLayout detail_page;
	private RelativeLayout step_page;
	private ScrollView help_menu;
	private Button nextOrBackToDetails;
	
	private HelpActivity view;

	public HelpController(HelpActivity view) {
		this.view = view;
		
		// page 1
		help_menu = (ScrollView) view.findViewById(R.id.instance_list_scroll_view);
		
		// page 2
		detail_page = (RelativeLayout) view.findViewById(R.id.detail_page);
		detail_header = (TextView) view.findViewById(R.id.detail_header);
		detail_help = (TextView) view.findViewById(R.id.detail_help);

		// page 3
		step_page = (RelativeLayout) view.findViewById(R.id.step_page);
		slideShow = (ImageView) view.findViewById(R.id.slide_show);
		
		nextOrBackToDetails = (Button) view.findViewById(R.id.next);
		
		help_header = new String[] {
			"Quick dial",
			"View data as list",
			"View detailed information",
			"Save contact",
			"Account",
			"Contribute information",
			"Update local data"
		};
		
		help_detail = new String[]{
			"Quick dial is the main feature of SiNoDar mobile app , which is able to calculate and auto-dial the nearest instance from your current location. Use this feature to call for help in emergency situation.",
			"SiNoDar provides data of many instance, that can be accessed from SiNoDar mobile app. SiNoDar mobile app will shows its data by list of instance, from the nearest to the farthest position from your current location.",
			"For more detailed information, select target instance from SiNoDar mobile app instance data list. SiNoDar mobile app will retrieve detailed information regarding selected instance, and shows them in no time.",
			"Each and every instance has a primary phone number, which can be saved to phone, by selecting menu Save contact from action bar in detailed information page.",
			"Every user of SiNoDar mobile app can create an account. Creation of an account will require the creator to fill a register form, this form is accessible from login page. Owning an account will give the owner many advantages, some of them are the ability to contribute building our database, by providing new data, and help other users by verifying our data, and send an edit request to our server.",
			"Every user who has created an account, and login into SiNoDar mobile app, able to makes requests for new instance or requests to edit data of any instance in SiNoDar database. Our admin will check each request, and update our database daily. Every contribution of data is crucial to help SiNoDar in providing the most actual data towards SiNoDar mobile app user. Never hesitate to contribute, because your contribution is help for others.",
			"SiNoDar mobile app will create a local data to ensure its performance when quick dial is used. SiNoDar mobile app will check for instance location from local data, and start to dial the nearest instance from your current location. With this local data, quick dial will still be able to give help to SiNoDar mobile app users, even if your device's internet connection is not stable. However, SiNoDar mobile app will not update its local data automatically, and you have to update its local data manually."
		};
		
		quick_dial_img = new Integer[] {
				R.drawable.quick_1,
				R.drawable.quick_2,
				R.drawable.quick_3
		};
		
		view_data_list_img = new Integer[] {
				R.drawable.save_1,
				R.drawable.save_2,
				R.drawable.view_3
		};
		
		view_detail_img = new Integer[] {
				R.drawable.save_1,
				R.drawable.save_2,
				R.drawable.save_3,
				R.drawable.det_4,
				R.drawable.det_5
		};
		
		save_contact_img = new Integer[] {
				R.drawable.save_1,
				R.drawable.save_2,
				R.drawable.save_3,
				R.drawable.save_4
		};
		
		account_img = new Integer[] {
				R.drawable.acc_1,
				R.drawable.acc_2,
				R.drawable.acc_3,
				R.drawable.acc_4
		};
		
		contribute_img = new Integer[] {
				R.drawable.contr_1,
				R.drawable.contr_2,
				R.drawable.contr_3,
				R.drawable.contr_4
		};
		
		update_img = new Integer[] {
				R.drawable.update_1
		};
		
		content_package = new Integer[][] {
				quick_dial_img,
				view_data_list_img,
				view_detail_img,
				save_contact_img,
				account_img,
				contribute_img,
				update_img
		};

		workerThread = new HandlerThread("MainMenuController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
	}

	@Override
	public boolean handleMessage(int what, Object data) {
		switch (what) {
			// 1
			case QUICK_DIAL:
				whichPage = 2;
				whichMenu = 0;
				loadSecondPageData();
				initSecondPage();
				return true;

			// 2
			case VIEW_DATA_LIST:
				whichPage = 2;
				whichMenu = 1;
				loadSecondPageData();
				initSecondPage();
				return true;
			
			// 3
			case VIEW_DETAIL:
				whichPage = 2;
				whichMenu = 2;
				loadSecondPageData();
				initSecondPage();
				return true;
				
			// 4
			case SAVE_CONTACT:
				whichPage = 2;
				whichMenu = 3;
				loadSecondPageData();
				initSecondPage();
				return true;
				
			// 5
			case ACCOUNT:
				whichPage = 2;
				whichMenu = 4;
				loadSecondPageData();
				initSecondPage();
				return true;
				
			// 6
			case CONTRIBUTE_INFORMATION:
				whichPage = 2;
				whichMenu = 5;
				loadSecondPageData();
				initSecondPage();
				return true;
				
			// 7
			case UPDATE_DATA:
				whichPage = 2;
				whichMenu = 6;
				loadSecondPageData();
				initSecondPage();
				return true;

			// 8
			case BACK:
				whichAction();
				return true;
				
			// 9
			case NEXT:
				whichPage = 3;
				if ((whichSteps+1) >= content_package[whichMenu].length) {
					whichSteps = 0;
					whichAction();
				} else {
					checkStepMax();
					whichSteps = whichSteps+1;
					loadThirdPageData();
					initThirdPage();
				}
				return true;
				
			// 10
			case BACK_TO_MENU:
				whichPage = 1;
				initFirstPage();
				return true;
				
			// 11
			case NEXT_TO_STEPS:
				whichPage = 3;
				whichSteps = 0;
				if ((whichSteps+1) >= content_package[whichMenu].length) {
					isLastImg = true;
				} else {
					isLastImg = false;
				}
				loadThirdPageData();
				initThirdPage();
				return true;
		}
		return false;
	}
	
	private void checkStepMax() {
		if ((whichSteps+2) >= content_package[whichMenu].length) {
			isLastImg = true;
		} else {
			isLastImg = false;
		}
	}
	
	public void setBackStepsFlipped() {
		if ((whichSteps+1) >= content_package[whichMenu].length) {
			isLastImg = true;
		} else {
			isLastImg = false;
		}
	}
	
	public void initThirdPage() {
		setStepGone();
		setDetailGone();
		setMenuGone();
		changeAlpha(step_page, 300, 0, 1);
		setStepVisible();
	}
	
	public void loadThirdPageData() {
		slideShow.setImageResource(content_package[whichMenu][whichSteps]);
	}
	
	public void initSecondPage() {
		setMenuGone();
		setDetailGone();
		setStepGone();
		changeAlpha(detail_page, 300, 0, 1);
		setDetailVisible();
	}
	
	public void loadSecondPageData() {
		detail_header.setText(help_header[whichMenu]);
		detail_help.setText(help_detail[whichMenu]);
	}
	
	public void initFirstPage() {
		setDetailGone();
		setMenuGone();
		setStepGone();
		changeAlpha(help_menu, 300, 0, 1);
		setMenuVisible();
	}
	
	private void whichAction() {
		if (whichSteps == 0) {
			whichPage = 2;
			loadSecondPageData();
			initSecondPage();
		} else {
			whichPage = 3;
			whichSteps = whichSteps-1;
			isLastImg = false;
			loadThirdPageData();
			initThirdPage();
		}
	}

	public int getWhichPage() {
		return whichPage;
	}
	
	public int getWhichMenu() {
		return whichMenu;
	}
	
	public int getWhichSteps() {
		return whichSteps;
	}
	
	public void setWhichPage(int p) {
		whichPage = p;
	}
	
	public void setWhichMenu(int m) {
		whichMenu = m;
	}
	
	public void setWhichStep(int s) {
		whichSteps = s;
	}
	
	private static void changeAlpha(View v, long duration, float from, float to) {
    	AlphaAnimation animateAlphaChange = new AlphaAnimation(from,to);
    	animateAlphaChange.setDuration(duration);
    	animateAlphaChange.setFillAfter(true);
    	v.startAnimation(animateAlphaChange);
    }
	
	public void setMenuVisible() {
		help_menu.setVisibility(View.VISIBLE);
		view.findViewById(R.id.help_txt).setVisibility(View.VISIBLE);
		view.findViewById(R.id.quick_dial).setVisibility(View.VISIBLE);
		view.findViewById(R.id.view_data_list).setVisibility(View.VISIBLE);
		view.findViewById(R.id.view_detail).setVisibility(View.VISIBLE);
		view.findViewById(R.id.save_contact).setVisibility(View.VISIBLE);
		view.findViewById(R.id.account).setVisibility(View.VISIBLE);
		view.findViewById(R.id.contribute_information).setVisibility(View.VISIBLE);
		view.findViewById(R.id.update_data).setVisibility(View.VISIBLE);
		view.findViewById(R.id.menu_container).setVisibility(View.VISIBLE);
	}
	
	public void setMenuGone() {
		help_menu.setVisibility(View.GONE);
		view.findViewById(R.id.help_txt).setVisibility(View.GONE);
		view.findViewById(R.id.quick_dial).setVisibility(View.GONE);
		view.findViewById(R.id.view_data_list).setVisibility(View.GONE);
		view.findViewById(R.id.view_detail).setVisibility(View.GONE);
		view.findViewById(R.id.save_contact).setVisibility(View.GONE);
		view.findViewById(R.id.account).setVisibility(View.GONE);
		view.findViewById(R.id.contribute_information).setVisibility(View.GONE);
		view.findViewById(R.id.update_data).setVisibility(View.GONE);
		view.findViewById(R.id.menu_container).setVisibility(View.GONE);
	}
	
	public void setStepGone() {
		step_page.setVisibility(View.GONE);
		slideShow.setVisibility(View.GONE);
		view.findViewById(R.id.back).setVisibility(View.GONE);
		view.findViewById(R.id.next).setVisibility(View.GONE);
		view.findViewById(R.id.slide_show_container).setVisibility(View.GONE);
		view.findViewById(R.id.slide_controller).setVisibility(View.GONE);
	}
	
	public void setStepVisible() {
		step_page.setVisibility(View.VISIBLE);
		slideShow.setVisibility(View.VISIBLE);
		view.findViewById(R.id.back).setVisibility(View.VISIBLE);
		view.findViewById(R.id.slide_show_container).setVisibility(View.VISIBLE);
		view.findViewById(R.id.slide_controller).setVisibility(View.VISIBLE);
		view.findViewById(R.id.next).setVisibility(View.VISIBLE);
		if (isLastImg == false) {
			nextOrBackToDetails.setText("Next");
		} else {
			nextOrBackToDetails.setText("Back to details");
		}
	}
	
	public void setDetailGone() {
		detail_page.setVisibility(View.GONE);
		detail_header.setVisibility(View.GONE);
		detail_help.setVisibility(View.GONE);
		view.findViewById(R.id.detail_container).setVisibility(View.GONE);
		view.findViewById(R.id.back_to_menu).setVisibility(View.GONE);
		view.findViewById(R.id.next_to_steps).setVisibility(View.GONE);
		view.findViewById(R.id.detail_controller).setVisibility(View.GONE);
		view.findViewById(R.id.detail_scroll_view).setVisibility(View.GONE);
	}
	
	public void setDetailVisible() {
		detail_page.setVisibility(View.VISIBLE);
		detail_header.setVisibility(View.VISIBLE);
		detail_help.setVisibility(View.VISIBLE);
		view.findViewById(R.id.detail_container).setVisibility(View.VISIBLE);
		view.findViewById(R.id.back_to_menu).setVisibility(View.VISIBLE);
		view.findViewById(R.id.next_to_steps).setVisibility(View.VISIBLE);
		view.findViewById(R.id.detail_controller).setVisibility(View.VISIBLE);
		view.findViewById(R.id.detail_scroll_view).setVisibility(View.VISIBLE);
	}
}
