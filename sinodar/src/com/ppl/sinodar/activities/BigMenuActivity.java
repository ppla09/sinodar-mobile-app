package com.ppl.sinodar.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ppl.sinodar.SinodarApp;
import com.ppl.sinodar.animations.Gate;
import com.ppl.sinodar.controllers.BigMenuController;

public class BigMenuActivity extends ActionBarActivity {

	public static final String INSTANCE_CODE = "instanceCode";
	public static final String SHOW_TOAST_UNSTABLE_INTERNET = "toastUnstableInternet";
	
	private static final String GATE_STATUS = "thisIsGate";
	
	private BigMenuController controller;
	private int instanceCode;
	
	private ImageButton quickDialBtn;
	private ImageButton viewListBtn;

	private int x2, x3;
	private boolean isOpened, isBackPressed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		instanceCode = getIntent().getIntExtra(INSTANCE_CODE, -1);
		if (instanceCode == SinodarApp.HOSPITAL) setContentView(R.layout.activity_bigmenu_hospital);
		else if (instanceCode == SinodarApp.FIRE_STATION) setContentView(R.layout.activity_bigmenu_firestation);
		else if (instanceCode == SinodarApp.POLICE_STATION) setContentView(R.layout.activity_bigmenu_policestation);

		if(getIntent().getBooleanExtra(SHOW_TOAST_UNSTABLE_INTERNET, false)) {
			Toast.makeText(this, R.string.unstable_internet_connection, Toast.LENGTH_SHORT).show();
			getIntent().removeExtra(SHOW_TOAST_UNSTABLE_INTERNET);
		}
		
        int originalPos2[] = new int[2];
		findViewById(R.id.gate_right).getLocationOnScreen(originalPos2);
		x2 = originalPos2[1];
        
        int originalPos3[] = new int[2];
        findViewById(R.id.middle_loading).getLocationOnScreen(originalPos3);
        x3 = originalPos3[1];
        
        Gate.initXGate(x2, x3);
		
		controller = new BigMenuController(BigMenuActivity.this);
		
		quickDialBtn = (ImageButton) findViewById(R.id.buttonDial);
		viewListBtn = (ImageButton) findViewById(R.id.buttonView);
		
		quickDialBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.instance_list_scroll_view).setVisibility(View.GONE);
				Gate.crossfadeAkhir();
				Gate.initCloseGate(BigMenuActivity.this);
				Gate.getLastMove().setAnimationListener(new AnimationListener(){
		            public void onAnimationStart(Animation a){}
		            public void onAnimationRepeat(Animation a){}
		            public void onAnimationEnd(Animation a){
		            	controller.handleMessage(BigMenuController.QUICK_DIAL, instanceCode);
		            }
		        });
				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				setGateStatus(false);
			}
		});

		viewListBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.instance_list_scroll_view).setVisibility(View.GONE);
				Gate.crossfadeAkhir();
				Gate.initCloseGate(BigMenuActivity.this);
				Gate.getLastMove().setAnimationListener(new AnimationListener(){
		            public void onAnimationStart(Animation a){}
		            public void onAnimationRepeat(Animation a){}
		            public void onAnimationEnd(Animation a){
		            	controller.handleMessage(BigMenuController.VIEW_INSTANCE_LIST, instanceCode);
		            }
		        });
				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				setGateStatus(false);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		Gate.initXGate(x2, x3);
		Gate.initGate(BigMenuActivity.this, isOpened);
		if (isOpened == false) {
			Gate.openGate(this,(ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			setGateStatus(true);
		}
	}

	@Override
	public void onBackPressed() {
		if (isBackPressed == false) {
			Gate.crossfadeAkhir();
			Gate.initCloseGate(BigMenuActivity.this);
			Gate.getLastMove().setAnimationListener(new AnimationListener(){
	            public void onAnimationStart(Animation a){}
	            public void onAnimationRepeat(Animation a){}
	            public void onAnimationEnd(Animation a){
	        		BigMenuActivity.super.onBackPressed();
	            }
	        });
			Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			isBackPressed = true;
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(GATE_STATUS, isOpened);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.getBoolean(GATE_STATUS)) {
			setGateStatus(true);
		} else {
			setGateStatus(false);
		}
	}


	private void setGateStatus(boolean is) {
		isOpened = is;
	}
}