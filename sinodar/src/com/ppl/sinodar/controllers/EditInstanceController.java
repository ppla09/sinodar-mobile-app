package com.ppl.sinodar.controllers;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.Toast;

import com.ppl.sinodar.activities.EditInstanceActivity;
import com.ppl.sinodar.activities.InstanceDetailActivity;
import com.ppl.sinodar.activities.LocationPickerActivity;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.daos.InstanceDao;
import com.ppl.sinodar.helper.ImageHelper;
import com.ppl.sinodar.models.ViewedInstanceModel;

public class EditInstanceController extends Controller {

	/* code for handling message */
	public static final int LOAD_INITIAL_PICTURE = 1;
	public static final int LOAD_INITIAL_MAP = 2;
	public static final int MESSAGE_CHOOSE_PICTURE = 3;
	public static final int MESSAGE_HANDLE_PICTURE = 4;
	public static final int MESSAGE_SET_INSTANCE_LOCATION = 5;
	public static final int MESSAGE_HANDLE_LOCATION_PICKED = 6;
	public static final int MESSAGE_SUBMIT_DATA = 7;
	
	private EditInstanceActivity view;
	private String username;
	private ViewedInstanceModel oldData;
	private double chosenLatitude, chosenLongitude;
	private Uri chosenImageUri = null; 
	private boolean hasLocationMoved = false;
	
	public EditInstanceController(EditInstanceActivity view, String username, ViewedInstanceModel oldData) {
		this.view = view;
		this.username = username;
		this.oldData = oldData;
		
		workerThread = new HandlerThread("EditInstanceController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
	}
	
	public Uri getChosenImageUri() { return chosenImageUri; }
	public boolean getHasInstanceLocationMoved() { return hasLocationMoved; }
	public double getChosenInstanceLatitude() { return chosenLatitude; }
	public double getChosenInstanceLongitude() { return chosenLongitude; }

	@SuppressWarnings("unchecked")
	@Override
	public boolean handleMessage(int what, Object data) {
		switch(what) {
			case LOAD_INITIAL_PICTURE:
				loadInitialPicture((String) data);
				return true;
				
			case LOAD_INITIAL_MAP:
				loadInitialMap();
				return true;
				
			case MESSAGE_CHOOSE_PICTURE:
				chooseInstancePicture();
				return true;
				
			case MESSAGE_HANDLE_PICTURE:
				handleChosenPicture((Uri) data);
				return true;
				
			case MESSAGE_SET_INSTANCE_LOCATION:
				setInstanceLocation();
				return true;
				
			case MESSAGE_HANDLE_LOCATION_PICKED:
				handleLocationPicked((ArrayList<Double>) data);
				return true;
				
			case MESSAGE_SUBMIT_DATA:
				submitInstanceDataEdit((ArrayList<Object>) data);
				return true;
		}
		return false;
	}

	private void loadInitialPicture(final String picPath) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				if(picPath != null) {
					Bitmap bm = ImageHelper.readTempImageFromStorage(view, EditInstanceActivity.TEMP_IMAGE_FILENAME);
					view.setInstancePicture(bm);
				}
			}
		});
	}
	
	private void loadInitialMap() {
		double latitude = Math.toDegrees(oldData.getLatitude());
		double longitude = Math.toDegrees(oldData.getLongitude());
		
		int partScreen = ImageHelper.getDeviceWidth(view) * 6 / 7;
        if (partScreen < 400) partScreen = 400;
        
        String url = "http://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude
        		+ "&zoom=15&size=" + partScreen + "x" + partScreen + "&markers=color:red%7C" + latitude 
        		+ "," + longitude + "&sensor=false&key=AIzaSyCYXhc_OOotrnbx5T-wNWUPak81c7f6tno";
        
        view.onLoadingInstanceMap();
        Bitmap image = ImageHelper.getImage(url);
        view.onLoadedInstanceMap(image);
	}
	
	private void chooseInstancePicture() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				Intent selectPhoto = new Intent(Intent.ACTION_GET_CONTENT);
				selectPhoto.setType("image/*");
				
				Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

				Intent chooserIntent = Intent.createChooser(selectPhoto, "Select or take a new picture");
				chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {takePhoto});

				view.startActivityForResult(chooserIntent, MESSAGE_CHOOSE_PICTURE);
			}
		});
	}
	
	private void handleChosenPicture(final Uri imageUri) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				chosenImageUri = imageUri;
				view.setInstancePicture(ImageHelper.getCorrectlyOrientedImage(view, imageUri));
			}
		});
	}
	
	private void setInstanceLocation() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(view, LocationPickerActivity.class);
				
				// put old marker (if any)
				synchronized((Boolean) hasLocationMoved) {
					if(hasLocationMoved) {
						intent.putExtra(LocationPickerActivity.LATITUDE, chosenLatitude);
						intent.putExtra(LocationPickerActivity.LONGITUDE, chosenLongitude);
					} else {
						intent.putExtra(LocationPickerActivity.LATITUDE, Math.toDegrees(oldData.getLatitude()));
						intent.putExtra(LocationPickerActivity.LONGITUDE, Math.toDegrees(oldData.getLongitude()));
					}
				}

				view.startActivityForResult(intent, MESSAGE_SET_INSTANCE_LOCATION);
			}
		});
	}
	
	private void handleLocationPicked(final ArrayList<Double> locationPicked) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				synchronized((Boolean) hasLocationMoved) {
					hasLocationMoved = true;
					chosenLatitude  = locationPicked.get(0);	// by convention: latitude  = idx 0
					chosenLongitude = locationPicked.get(1);	//				  longitude = idx 1
					
					loadMap(chosenLatitude, chosenLongitude, "red");
				}
			}
		});
	}
	

	private boolean validateField(String name, String addr, ArrayList<String> phoneNum, int chosenMain, String desc) {
		boolean valid = true;
		
		/* field error */
		if(desc.length() > 1500) {
			view.setInstanceDescFieldError(R.string.error_field_too_long_1500);
			valid = false;
		}
		
		if(chosenMain != -1 && phoneNum.get(chosenMain).length() == 0) {
			view.setPhoneNumberFieldError(chosenMain, R.string.error_field_required);
			valid = false;
		} else for(int i = 0; i < phoneNum.size(); i++) if(phoneNum.get(i).length() > 20) {
			view.setPhoneNumberFieldError(i, R.string.error_field_too_long_20);
			valid = false;
		} else {
			boolean unique = true;
			for(int j = 1; j < phoneNum.size() && unique; j++) if(!phoneNum.get(j).isEmpty()) {
				for(int k = j - 1; k >= 0 && unique; k--) unique = !(phoneNum.get(j).equals(phoneNum.get(k)));
				
				if(!unique) {
					view.setPhoneNumberFieldError(i, R.string.error_phonenum_not_unique);
					valid = false;
				}
			}
		}
		
		if(addr.length() > 200) {
			view.setInstanceAddrFieldError(R.string.error_field_too_long_200);
			valid = false;
		}
		
		if(TextUtils.isEmpty(name)) {
			view.setInstanceNameFieldError(R.string.error_field_required);
			valid = false;
		} else if(name.length() > 200) {
			view.setInstanceNameFieldError(R.string.error_field_too_long_200);
			valid = false;
		}
		
		/* Toast error */
		if(valid && chosenMain == -1) {
			Toast.makeText(view, R.string.main_phone_not_chosen, Toast.LENGTH_SHORT).show();
			valid = false;
		}
		
		return valid;
	}
	
	private String checkChangedStr(String newStr, String oldStr) {
		if(newStr.length() != oldStr.length() || !newStr.equals(oldStr)) return newStr;
		return null;
	}
	
	private Integer checkChangedNum(int newIdx, String newMainPhone, String oldMainPhone) {
		if(newMainPhone.length() != oldMainPhone.length() || !newMainPhone.equals(oldMainPhone)) return newIdx;
		return null;
	}
	
	private ArrayList<String> checkChangedOther(int newMainIdx, ArrayList<String> newPhoneNum, ArrayList<String> oldOtherPhone) {
		boolean changed = false;
		boolean flag[] = new boolean[oldOtherPhone.size()];
		
		for(int i = 0; i < oldOtherPhone.size(); i++) flag[i] = false;
		for(int i = 0; i < newPhoneNum.size() && !changed; i++) if(i != newMainIdx && !newPhoneNum.get(i).isEmpty()) {
			boolean found = false;
			for(int j = 0; j < oldOtherPhone.size() && !found; j++) 
				if(newPhoneNum.get(i).equals(oldOtherPhone.get(j))) flag[j] = found = true;
			
			if(!found) changed = true;
		}
		
		if(!changed) {
			int trueCnt = 0;
			for(int i = 0; i < flag.length; i++) trueCnt += (flag[i] ? 1 : 0);
			if(trueCnt < flag.length) changed = true;
		}
		
		return (changed ? newPhoneNum : null);
	}
	
	private void submitInstanceDataEdit(final ArrayList<Object> data) {
		workerHandler.post(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				String name = (String) data.get(0);								// by convention: name = idx 0
				String addr = (String) data.get(1);								//			      addr = idx 1
				ArrayList<String> phoneNum = (ArrayList<String>) data.get(2);	//			      phone = idx 2
				int chosenMain = (Integer) data.get(3);							//			      chosenMain = idx 3
				String desc = (String) data.get(4);								//			      desc = idx 4
				
				if(!validateField(name, addr, phoneNum, chosenMain, desc)) return;
				
				int changedCnt = 0;
				String sendName  = checkChangedStr(name, oldData.getName()); if(sendName != null) changedCnt++;
				String sendAddr  = checkChangedStr(addr, oldData.getAddress()); if(sendAddr != null) changedCnt++;
				String sendDesc  = checkChangedStr(desc, oldData.getDescription()); if(sendDesc != null) changedCnt++;
				Double sendLat   = hasLocationMoved ? chosenLatitude : null; if(sendLat != null) changedCnt++;
				Double sendLong  = hasLocationMoved ? chosenLongitude : null; if(sendLong != null) changedCnt++;
				Integer sendMain = checkChangedNum(chosenMain, phoneNum.get(chosenMain), oldData.getMainPhoneNumber()); if(sendMain != null) changedCnt++; 
				ArrayList<String> sendOther = checkChangedOther(chosenMain, phoneNum, oldData.getOtherPhoneNumber()); if(sendOther != null) changedCnt++;
				if(chosenImageUri != null) changedCnt++; 
				
				if(changedCnt == 0) {
					Toast.makeText(view, R.string.no_changed_field, Toast.LENGTH_SHORT).show();
					return;
				}
				
				view.onLoading();
				boolean success;
				
				try { 
					success = InstanceDao.postEditInstance(view, oldData.getId(), username, (sendName == null ? oldData.getName() : sendName), 
							sendName, sendAddr, sendDesc, sendLat, sendLong, sendMain, chosenMain, sendOther, chosenImageUri);
				} catch (Exception e) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.unstable_internet_connection_edit, Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(!success) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.failed_to_edit, Toast.LENGTH_SHORT).show();
				} else {
					Intent intent = new Intent(view, InstanceDetailActivity.class);
					intent.putExtra(InstanceDetailActivity.INSTANCE_ID, oldData.getId());
					intent.putExtra(InstanceDetailActivity.SHOW_TOAST_SUCCESS_EDIT, true);
					
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					view.startActivity(intent);
				}
			}
		});
	}
	
	private void loadMap(double latitude, double longitude, String color) {
		int size = ImageHelper.getDeviceWidth(view);
		String url = "http://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude
				+ "&zoom=15&size=" + size + "x" + size + "&markers=color:" + color + "%7C" + latitude + "," + longitude  
				+ "&sensor=false&key=AIzaSyBieC9-5mcClPiCC_zd9cRFDJR-wc7R07I";
		
        view.onLoadingInstanceMap();
        Bitmap image = ImageHelper.getImage(url);
        view.onLoadedInstanceMap(image);
	}
}
