package com.ppl.sinodar.animations;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;

import com.ppl.sinodar.activities.R;

public class Gate {

	private static int posisiAwalMid;
	private static int posisiAwalGateKanan;
	private static TranslateAnimation openKiri;
	private static TranslateAnimation openKanan;
	private static TranslateAnimation moveKanan;
	private static TranslateAnimation closeKiri;
	private static TranslateAnimation closeKanan;
	private static TranslateAnimation moveKiri;
	private static View content;
	private static View loading;
	
	public static void initXGate(int x2, int x3) {
        posisiAwalGateKanan = x2;
        posisiAwalMid = x3;
	}
	
	public static void initGate(Activity act, boolean isOpened) {
		content = act.findViewById(R.id.instance_list_scroll_view);
        loading = act.findViewById(R.id.loading);
        
        if (isOpened == false) {
        	content.setVisibility(View.GONE);
        }
	}
	
	public static void openGate(Activity act, View view1, View view2, View view3)
    {
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics( dm );

        int halfScreen = dm.widthPixels/2;

        openKiri = new TranslateAnimation(0, -halfScreen, 0, 0);
        openKiri.setDuration(1000);
        openKiri.setFillAfter(true);
        
        openKanan = new TranslateAnimation(posisiAwalGateKanan, posisiAwalGateKanan+halfScreen, 0, 0);
        openKanan.setDuration(1000);
        openKanan.setFillAfter(true);
        
        moveKanan = new TranslateAnimation(posisiAwalMid, posisiAwalMid+halfScreen, 0, 0);
        moveKanan.setDuration(1000);
        moveKanan.setFillAfter(true);

        moveKanan.setAnimationListener(new AnimationListener(){
            public void onAnimationStart(Animation a){}
            public void onAnimationRepeat(Animation a){}
            public void onAnimationEnd(Animation a){
            	crossfadeAwal();
            }
        });
        
        view1.startAnimation(openKiri);
        view2.startAnimation(openKanan);
        view3.startAnimation(moveKanan);
    }
    
	public static void initCloseGate(Activity act)
    {
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics( dm );

        int halfScreen = dm.widthPixels/2;

        closeKiri = new TranslateAnimation(-halfScreen, 0, 0, 0);
        closeKiri.setDuration(300);
        closeKiri.setFillAfter(true);
        
        closeKanan = new TranslateAnimation(halfScreen+posisiAwalGateKanan, posisiAwalGateKanan, 0, 0);
        closeKanan.setDuration(300);
        closeKanan.setFillAfter(true);
        
        moveKiri = new TranslateAnimation(halfScreen-posisiAwalMid, posisiAwalMid, 0, 0);
        moveKiri.setDuration(300);
        moveKiri.setFillAfter(true);
    }
	
	public static void startCloseGate(View view1, View view2, View view3) {
        view1.startAnimation(closeKiri);
        view2.startAnimation(closeKanan);
        view3.startAnimation(moveKiri);
	}
    
	public static void crossfadeAwal() {
    	changeAlpha(content, 0, 1.0f, 0.0f);
    	content.setVisibility(View.VISIBLE);
    	changeAlpha(content, 300, 0.0f, 1.0f);
    	
    	hideGate();
    }
    
	public static void crossfadeAkhir() {
    	changeAlpha(loading, 0, 1.0f, 0.0f);
    	loading.setVisibility(View.VISIBLE);
    	changeAlpha(loading, 250, 0.0f, 1.0f);
    	
    	hideContent();
    }
    
	private static void changeAlpha(View v, long duration, float from, float to) {
    	AlphaAnimation animateAlphaChange = new AlphaAnimation(from,to);
    	animateAlphaChange.setDuration(duration);
    	animateAlphaChange.setFillAfter(true);
    	v.startAnimation(animateAlphaChange);
    }
    
	private static void hideGate() {
    	AlphaAnimation animateAlphaChange = new AlphaAnimation(1.0f,0.0f);
    	animateAlphaChange.setDuration(300);
    	animateAlphaChange.setFillAfter(true);
    	animateAlphaChange.setAnimationListener(new AnimationListener(){
            public void onAnimationStart(Animation a){}
            public void onAnimationRepeat(Animation a){}
            public void onAnimationEnd(Animation a){
            	loading.setVisibility(View.GONE);
            }
        });
    	loading.startAnimation(animateAlphaChange);
    }
    
	private static void hideContent() {
    	AlphaAnimation animateAlphaChange = new AlphaAnimation(1.0f,0.0f);
    	animateAlphaChange.setDuration(300);
    	animateAlphaChange.setFillBefore(true);
    	animateAlphaChange.setAnimationListener(new AnimationListener(){
            public void onAnimationStart(Animation a){}
            public void onAnimationRepeat(Animation a){}
            public void onAnimationEnd(Animation a){
            	content.setVisibility(View.GONE);
            }
        });
    	content.startAnimation(animateAlphaChange);
    }
	
	public static TranslateAnimation getLastMove() {
		return moveKiri;
	}
	
	public static void changeLoadScreen(View v) {
		loading = v;
	}
	
	public static void changeContent(View v) {
		content = v;
	}
}
