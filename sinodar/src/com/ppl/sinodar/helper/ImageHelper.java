package com.ppl.sinodar.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.ppl.sinodar.SinodarApp;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;

/**
 *	taken from 
 *  - http://stackoverflow.com/questions/3647993/android-bitmaps-loaded-from-gallery-are-rotated-in-imageview
 *  - http://stackoverflow.com/questions/2991110/android-how-to-stretch-an-image-to-the-screen-width-while-maintaining-aspect-ra
 *  - http://stackoverflow.com/questions/19594152/compress-camera-image-before-upload
 */
public class ImageHelper {

	public static int getDeviceWidth(Activity view) {
		DisplayMetrics dm = new DisplayMetrics();
        view.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
	}
	
	public static Bitmap getImage(String url) {
		Bitmap image = null;
		try {
			InputStream in = new URL(url).openStream();
			image = BitmapFactory.decodeStream(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}
	
	public static void saveTempImageToStorage(Bitmap bmp, String filename) {
		try {
			FileOutputStream outputStream = SinodarApp.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
			bmp.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
			outputStream.flush();
			outputStream.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Bitmap readTempImageFromStorage(Activity view, String filename) {
		Bitmap ret = null;
		
		try {
			InputStream is = view.openFileInput(filename);
		    ret = BitmapFactory.decodeStream(is);
		    is.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public static InputStream compressImageFromUri(Context context, Uri picUri, CompressFormat format, int quality) {
		Bitmap bmp = getCorrectlyOrientedImage(context, picUri);
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bmp.compress(format, quality, bos);
		return new ByteArrayInputStream(bos.toByteArray());
	}
	
	private static int getOrientation(Context context, Uri photoUri) {
	    /* it's on the external media. */
	    Cursor cursor = context.getContentResolver().query(photoUri, 
	    		new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);

	    if (cursor.getCount() != 1) return -1;
	    cursor.moveToFirst();
	    return cursor.getInt(0);
	}
	
	public static Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) {
	    Bitmap srcBitmap = null;
		
		try {
			InputStream is = context.getContentResolver().openInputStream(photoUri);
		    BitmapFactory.Options dbo = new BitmapFactory.Options();
		    dbo.inJustDecodeBounds = true;
		    BitmapFactory.decodeStream(is, null, dbo);
		    is.close();
	
		    int rotatedWidth, rotatedHeight;
		    int orientation = getOrientation(context, photoUri);
	
		    if (orientation == 90 || orientation == 270) {
		        rotatedWidth = dbo.outHeight;
		        rotatedHeight = dbo.outWidth;
		    } else {
		        rotatedWidth = dbo.outWidth;
		        rotatedHeight = dbo.outHeight;
		    }
	
			is = context.getContentResolver().openInputStream(photoUri);
		    int maxImageDimension = getDeviceWidth((Activity) context);
		      
		    if (rotatedWidth > maxImageDimension || rotatedHeight > maxImageDimension) {
		        float widthRatio = ((float) rotatedWidth) / ((float) maxImageDimension);
		        float heightRatio = ((float) rotatedHeight) / ((float) maxImageDimension);
		        float maxRatio = Math.max(widthRatio, heightRatio);
	
		        // Create the bitmap from file
		        BitmapFactory.Options options = new BitmapFactory.Options();
		        options.inSampleSize = (int) maxRatio;
		        srcBitmap = BitmapFactory.decodeStream(is, null, options);
		    } else {
		        srcBitmap = BitmapFactory.decodeStream(is);
		    }
		    is.close();
	
		    /*
		     * if the orientation is not 0 (or -1, which means we don't know), we
		     * have to do a rotation.
		     */
		    if (orientation > 0) {
		        Matrix matrix = new Matrix();
		        matrix.postRotate(orientation);
	
		        srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
		    } 
		} catch(IOException e) {
	    	e.printStackTrace();
	    }
		
		return srcBitmap;
	}
}
