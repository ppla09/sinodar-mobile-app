package com.ppl.sinodar.activities;

import com.ppl.sinodar.animations.Gate;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;


public class AboutUsActivity extends ActionBarActivity {

	private static final String GATE_STATUS = "thisIsGate";

	private int x2, x3;
	private boolean isOpened, isBackPressed;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);

        int originalPos2[] = new int[2];
		findViewById(R.id.gate_right).getLocationOnScreen(originalPos2);
		x2 = originalPos2[1];
        
        int originalPos3[] = new int[2];
        findViewById(R.id.middle_loading).getLocationOnScreen(originalPos3);
        x3 = originalPos3[1];
        
        Gate.initXGate(x2, x3);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Gate.initXGate(x2, x3);
		Gate.initGate(AboutUsActivity.this, isOpened);
		if (isOpened == false) {
			Gate.openGate(this,(ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			setGateStatus(true);
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isBackPressed == false) {
			Gate.crossfadeAkhir();
			Gate.initCloseGate(AboutUsActivity.this);
			Gate.getLastMove().setAnimationListener(new AnimationListener(){
	            public void onAnimationStart(Animation a){}
	            public void onAnimationRepeat(Animation a){}
	            public void onAnimationEnd(Animation a){
	            	AboutUsActivity.super.onBackPressed();
	            }
	        });
			Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			isBackPressed = true;
		}
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(GATE_STATUS, isOpened);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.getBoolean(GATE_STATUS)) setGateStatus(true); else setGateStatus(false);
	}

	private void setGateStatus(boolean is) {
		isOpened = is;
	}
}
