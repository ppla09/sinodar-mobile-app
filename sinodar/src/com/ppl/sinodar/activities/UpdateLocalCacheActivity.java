package com.ppl.sinodar.activities;

import com.ppl.sinodar.controllers.UpdateLocalCacheController;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class UpdateLocalCacheActivity extends ActionBarActivity {

	UpdateLocalCacheController controller;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_local_cache);
		
		controller = new UpdateLocalCacheController(this);
		controller.handleMessage(UpdateLocalCacheController.UPDATE_LOCAL_CACHE);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.dispose();
	}
}
