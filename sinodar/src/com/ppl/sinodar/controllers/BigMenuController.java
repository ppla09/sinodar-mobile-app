package com.ppl.sinodar.controllers;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.widget.Toast;

import com.ppl.sinodar.activities.BigMenuActivity;
import com.ppl.sinodar.activities.InstanceListActivity;
import com.ppl.sinodar.activities.QuickDialActivity;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.helper.ConnectionHelper;

public class BigMenuController extends Controller {
	
	/* code for message handling */
	public static final int VIEW_INSTANCE_LIST = 1;
	public static final int QUICK_DIAL = 2;

	private BigMenuActivity view;

	public BigMenuController(BigMenuActivity view) {
		this.view = view;

		workerThread = new HandlerThread("BigMenuController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());

	}
	
	@Override
	public boolean handleMessage(int what, Object data) {
		switch (what) {
			case VIEW_INSTANCE_LIST:
				viewInstanceList((Integer) data);
				return true;
				
			case QUICK_DIAL:
				quickDial((Integer) data);
				return true;
		}
		return false;
	}

	private void viewInstanceList(int instanceCode) {
		if (!ConnectionHelper.isOnline()) {
			Toast.makeText(view, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
			return;
		}
		
		Intent intent = new Intent(view, InstanceListActivity.class);
		intent.putExtra(InstanceListActivity.INSTANCE_CODE, instanceCode);
		view.startActivity(intent);
	}

	public void quickDial(int instanceCode) {
		Intent intent = new Intent(view, QuickDialActivity.class);
		intent.putExtra(QuickDialActivity.INSTANCE_CODE, instanceCode);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		view.startActivity(intent);
	}
}
