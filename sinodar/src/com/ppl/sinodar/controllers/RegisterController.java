package com.ppl.sinodar.controllers;

import java.util.ArrayList;
import java.util.regex.Pattern;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import android.widget.Toast;

import com.ppl.sinodar.activities.MainMenuActivity;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.activities.RegisterActivity;
import com.ppl.sinodar.daos.SessionDao;
import com.ppl.sinodar.helper.ConnectionHelper;
import com.ppl.sinodar.models.UserSessionModel;

public class RegisterController extends Controller {

	public static final int MESSAGE_USER_REGISTER = 1;
	
	private RegisterActivity view;
	
	public RegisterController(RegisterActivity view) {
		this.view = view;
		
		workerThread = new HandlerThread("LoginController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean handleMessage(int what, Object data) {
		switch(what) {
			case MESSAGE_USER_REGISTER: register((ArrayList<String>) data); 
										return true;
		}
		return false;
	}

	public boolean validateField(String username, String password, String email, String firstname, String lastname) {
		view.resetFieldError();
		boolean valid = true;
		
		if(lastname != null && lastname.length() > 100) {
			view.setUserLastNameFieldError(R.string.error_field_too_long_100);
			valid = false;
		}
		
		if(TextUtils.isEmpty(firstname)) {
			view.setUserFirstNameFieldError(R.string.error_field_required);
			valid = false;
		} else if(firstname.length() > 100) {
			view.setUserFirstNameFieldError(R.string.error_field_too_long_100);
			valid = false;
		}
		
		if(TextUtils.isEmpty(email)) {
			view.setEmailFieldError(R.string.error_field_required);
			valid = false;
		} else if(!Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", email)) {
			view.setEmailFieldError(R.string.error_invalid_email);
			valid = false;
		} else if(email.length() > 100) {
			view.setEmailFieldError(R.string.error_field_too_long_100);
			valid = false;
		}
		
		if(TextUtils.isEmpty(password)) {
			view.setPasswordFieldError(R.string.error_field_required);
			valid = false;
		} else if(password.length() > 100) {
			view.setPasswordFieldError(R.string.error_field_too_long_100);
			valid = false;
		}
		
		if(TextUtils.isEmpty(username)) {
			view.setUserNameFieldError(R.string.error_field_required);
			valid = false;
		} else if(username.length() > 100) {
			view.setUserNameFieldError(R.string.error_field_too_long_100);
			valid = false;
		}
		
		return valid;
	}
	
	private void register(final ArrayList<String> data) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				String username = data.get(0);	// by convention: username  -> idx 0
				String password = data.get(1);	//				  password  -> idx 1
				String email = data.get(2);		// 				  email	    -> idx 2
				String firstname = data.get(3);	//				  firstname	-> idx 3
				String lastname = data.get(4);	//				  lastname	-> idx 4
				if(lastname.equals("")) lastname = null;
				
				if(!validateField(username, password, email, firstname, lastname)) return;
				
				if(!ConnectionHelper.isOnline()) {
					Toast.makeText(view, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
					return;
				}
				
				view.onLoading();
				UserSessionModel model;
				try {
					model = SessionDao.postRegisterData(username, password, email, firstname, lastname);
				} catch (Exception e) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.unstable_internet_connection, Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(model == null) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.username_taken, Toast.LENGTH_SHORT).show();
					return;
				} else {
					SessionDao.saveUserSession(model);
					
					Intent intent = new Intent(view, MainMenuActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					view.startActivity(intent);
				}
			}
		});
	}
}
