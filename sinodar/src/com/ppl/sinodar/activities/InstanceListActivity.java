package com.ppl.sinodar.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ppl.sinodar.controllers.InstanceListController;
import com.ppl.sinodar.lists.InstanceListAdapter;

public class InstanceListActivity extends ActionBarActivity implements Handler.Callback {
	
	public static final String INSTANCE_CODE = "instanceCode";
	public static final String SHOW_TOAST_UNSTABLE_INTERNET = "unstableInt";
	public static final String SHOW_TOAST_DATABASE_ERROR = "dbError";
	
	private InstanceListController controller;
	private InstanceListAdapter adapter;
	private int instanceCode;
	
	private View listPage;
	private ListView instanceListHolder;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_instance_list);
		
		if(getIntent().getBooleanExtra(SHOW_TOAST_UNSTABLE_INTERNET, false)) {
			Toast.makeText(this, R.string.unstable_internet_connection, Toast.LENGTH_SHORT).show();
			getIntent().removeExtra(SHOW_TOAST_UNSTABLE_INTERNET);
		} else if(getIntent().getBooleanExtra(SHOW_TOAST_DATABASE_ERROR, false)) {
			Toast.makeText(this, R.string.database_error, Toast.LENGTH_SHORT).show();
			getIntent().removeExtra(SHOW_TOAST_DATABASE_ERROR);
		}
		
		controller = new InstanceListController(this);
		controller.addOutboxHandler(new Handler(this));
		
		listPage = findViewById(R.id.instance_list_view);
		instanceListHolder = (ListView) findViewById(R.id.instance_list_listview);
		instanceListHolder.setEmptyView(findViewById(R.id.instance_empty_tag));
		registerForContextMenu(instanceListHolder);
		
		adapter = new InstanceListAdapter(this, controller.getModel());
		instanceListHolder.setAdapter(adapter);
		instanceListHolder.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		@Override
    		public void onItemClick(AdapterView<?> a, View v, int pos, long l) {
    			controller.handleMessage(InstanceListController.GET_INSTANCE_DETAIL, adapter.getItem(pos).getId());
    		}
    	});
	
		instanceCode = getIntent().getIntExtra(INSTANCE_CODE, -1);
		controller.handleMessage(InstanceListController.POPULATE_LIST, instanceCode);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.dispose();
	}

	public void onLoading() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				listPage.setVisibility(View.GONE);
				findViewById(R.id.loading).setVisibility(View.VISIBLE);
				findViewById(R.id.loading2).setVisibility(View.GONE);
			}
		});
	}

	public void onResumeList() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				listPage.setVisibility(View.VISIBLE);
				findViewById(R.id.loading).setVisibility(View.GONE);
				findViewById(R.id.loading2).setVisibility(View.VISIBLE);
			}
		});
	}

	@Override
	public boolean handleMessage(Message arg0) {
		switch (arg0.what) {
			case InstanceListController.MESSAGE_MODEL_UPDATED:
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						adapter.notifyDataSetChanged();
					}
				});
				return true;
		}
		return false;
	}
}
