package com.ppl.sinodar.models;

public class UserSessionModel extends SimpleObservable<UserSessionModel> {
	
	private boolean isLogged = false;
	private String userName = "";
	private String userFirstName = "";
	
	public boolean getIsLogged() { return isLogged; }
	public String getUserName() { return userName; }
	public String getUserFirstName() { return userFirstName; }
	
	public void setIsLogged(boolean isLogged) {
		this.isLogged = isLogged;
		notifyObservers(this);
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
		notifyObservers(this);
	}
	
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
		notifyObservers(this);
	}
	
	@Override
	synchronized public UserSessionModel clone() {
		UserSessionModel model = new UserSessionModel();
		model.setIsLogged(isLogged);
		model.setUserName(userName);
		model.setUserFirstName(userFirstName);
		return model;
	}
	
	synchronized public void consume(UserSessionModel model) {
		this.isLogged = model.getIsLogged();
		this.userName = model.getUserName();
		this.userFirstName = model.getUserFirstName();
		notifyObservers(this);
	}
}
