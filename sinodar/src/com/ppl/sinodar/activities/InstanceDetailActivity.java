package com.ppl.sinodar.activities;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ppl.sinodar.controllers.InstanceDetailController;
import com.ppl.sinodar.models.InstanceModel;
import com.ppl.sinodar.models.OnChangeListener;
import com.ppl.sinodar.models.ViewedInstanceModel;

public class InstanceDetailActivity extends ActionBarActivity implements OnChangeListener<InstanceModel> {

	public static final String INSTANCE_ID = "instanceId";
	public static final String SHOW_TOAST_SUCCESS_EDIT = "toastSuccess";
	
	private static final String OVERWRITE_DIALOG_STATE = "overwrite";
	
	private InstanceDetailController controller;
	private int instanceId;
	
	public Context context = this;
	
	private ImageView picture, map;
	private TextView name, distance, addr, desc;
	private LinearLayout phonenum;
	private View detailPage, detailProgress, pictureProgress, mapProgress;
	private Dialog overwriteDialog;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_instance_detail);
		
		if(getIntent().getBooleanExtra(SHOW_TOAST_SUCCESS_EDIT, false)) {
			Toast.makeText(this, R.string.success_to_edit, Toast.LENGTH_SHORT).show();
			getIntent().removeExtra(SHOW_TOAST_SUCCESS_EDIT);
		}
		
		picture = (ImageView) findViewById(R.id.instance_detail_pic);
		name = (TextView) findViewById(R.id.instance_detail_name);
		distance = (TextView) findViewById(R.id.instance_detail_distance);
		addr = (TextView) findViewById(R.id.instance_detail_address);
		desc = (TextView) findViewById(R.id.instance_detail_desc);
		phonenum = (LinearLayout) findViewById(R.id.instance_detail_phone);
		map = (ImageView) findViewById(R.id.instance_detail_map);
		
		pictureProgress = findViewById(R.id.instance_detail_pic_progress_bar);
		mapProgress = findViewById(R.id.instance_detail_map_progress_bar);
		detailPage = findViewById(R.id.instance_detail_scroll_view);
		detailProgress = findViewById(R.id.loading);
		
		instanceId = getIntent().getIntExtra(INSTANCE_ID, -1);
		controller = new InstanceDetailController(this, instanceId);

		map.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				controller.handleMessage(InstanceDetailController.VIEW_MAP_ROAD);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.instance_detail, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		getMenuInflater().inflate(R.menu.instance_detail, menu);
		
		boolean isLogged = controller.getSession().getIsLogged();
		
		MenuItem editInstance = menu.findItem(R.id.actionbar_menu_editInstance);
		editInstance.setVisible(isLogged);
		
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.actionbar_menu_saveContact:
				controller.handleMessage(InstanceDetailController.MESSAGE_SAVE_CONTACT);
				return true;
				
			case R.id.actionbar_menu_editInstance:
				controller.handleMessage(InstanceDetailController.VIEW_EDIT_FORM);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.dispose();
	}
	
	@Override
	public void onChange(final InstanceModel model) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ViewedInstanceModel viewed = (ViewedInstanceModel) model;
				
				name.setText(viewed.getName());
				distance.setText(viewed.getBeautifulDistance());
				addr.setText(viewed.getAddress().length() == 0 ? "-" : viewed.getAddress());
				desc.setText(viewed.getDescription().length() == 0 ? "-" : viewed.getDescription());
				
				controller.handleMessage(InstanceDetailController.MESSAGE_LOAD_PICTURE);
				controller.handleMessage(InstanceDetailController.MESSAGE_LOAD_MAP);
				
				loadInstanceNumber(viewed.getMainPhoneNumber(), viewed.getOtherPhoneNumber());
			}
		});
	}

	public void onLoading() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				detailPage.setVisibility(View.GONE);
				detailProgress.setVisibility(View.VISIBLE);
			}
		});
	}
	
	public void onResumeDetail() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				detailPage.setVisibility(View.VISIBLE);
				detailProgress.setVisibility(View.GONE);
			}
		});
	}
	
	public void onLoadingInstancePic() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				picture.setVisibility(View.GONE);
				pictureProgress.setVisibility(View.VISIBLE);
			}
		});
	}
	
	public void onLoadedInstancePic(final Bitmap image) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				picture.setVisibility(View.VISIBLE);
				pictureProgress.setVisibility(View.GONE);
				
				picture.setImageBitmap(image);
			}
		});
	}
	
	public void onLoadingInstanceMap() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				map.setVisibility(View.GONE);
				mapProgress.setVisibility(View.VISIBLE);
			}
		});
	}
	
	public void onLoadedInstanceMap(final Bitmap image) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				map.setVisibility(View.VISIBLE);
				mapProgress.setVisibility(View.GONE);
				
				map.setImageBitmap(image);
			}
		});
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(OVERWRITE_DIALOG_STATE, (overwriteDialog != null && overwriteDialog.isShowing()));
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.getBoolean(OVERWRITE_DIALOG_STATE)) createOverwriteDialog();
	}
	
	public void createOverwriteDialog() {
		if (overwriteDialog != null && overwriteDialog.isShowing()) overwriteDialog.dismiss();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(getString(R.string.title_overwrite_confirmation));
		builder.setMessage(getString(R.string.overwrite_confirmation));
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				controller.handleMessage(InstanceDetailController.MESSAGE_OVERWRITE_CONFIRMED);
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				overwriteDialog.dismiss();
			}
		});

		overwriteDialog = builder.show();
	}
	
	private void loadInstanceNumber(final String mainNumber, final ArrayList<String> otherNumber) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				phonenum.removeAllViews();
				
				TextView mainHolder = new TextView(InstanceDetailActivity.this);
				mainHolder.setTextColor(getResources().getColor(R.color.dark_black));
				mainHolder.setText(mainNumber);
				phonenum.addView(mainHolder);
				
				for(int i = 0; i < otherNumber.size(); i++) {
					TextView otherHolder = new TextView(InstanceDetailActivity.this);
					otherHolder.setTextColor(getResources().getColor(R.color.dark_black));
					otherHolder.setText(otherNumber.get(i));
					phonenum.addView(otherHolder);
				}
			}
		});
	}
	
	public Bitmap getInstancePic() {
		return ((BitmapDrawable) picture.getDrawable()).getBitmap();
	}
}