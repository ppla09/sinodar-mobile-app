package com.ppl.sinodar.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ppl.sinodar.animations.Gate;
import com.ppl.sinodar.controllers.HelpController;

public class HelpActivity extends  ActionBarActivity {

	private static final String GATE_STATUS = "thisIsGate";
	private static final String PAGE_BANK = "pages";
	private static final String MENU_BANK = "menus";
	private static final String STEP_BANK = "steps";

	private HelpController controller;

	private Button quick_dial;
	private Button view_data_list;
	private Button view_detail;
	private Button save_contact;
	private Button account;
	private Button contribute_information;
	private Button update_data;
	private Button back_to_main;
	private Button back;
	private Button next;
	private Button back_to_menu;
	private Button next_to_steps;

	private int x2, x3;
	private int whichPage = 1;
	private int whichMenu = 0;
	private int whichStep = 0;
	private boolean isOpened, isBackPressed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

        int originalPos2[] = new int[2];
		findViewById(R.id.gate_right).getLocationOnScreen(originalPos2);
		x2 = originalPos2[1];
        
        int originalPos3[] = new int[2];
        findViewById(R.id.middle_loading).getLocationOnScreen(originalPos3);
        x3 = originalPos3[1];
        
        Gate.initXGate(x2, x3);
        
		controller = new HelpController(HelpActivity.this);
		
		isBackPressed = false;
		
		quick_dial = (Button) findViewById(R.id.quick_dial);
		view_data_list = (Button) findViewById(R.id.view_data_list);
		view_detail = (Button) findViewById(R.id.view_detail);
		save_contact = (Button) findViewById(R.id.save_contact);
		account = (Button) findViewById(R.id.account);
		contribute_information = (Button) findViewById(R.id.contribute_information);
		update_data = (Button) findViewById(R.id.update_data);
		back_to_main = (Button) findViewById(R.id.back_to_main_menu);
		back = (Button) findViewById(R.id.back);
		next = (Button) findViewById(R.id.next);
		back_to_menu = (Button) findViewById(R.id.back_to_menu);
		next_to_steps = (Button) findViewById(R.id.next_to_steps);

		quick_dial.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.QUICK_DIAL);
			}
		});
		
		view_data_list.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.VIEW_DATA_LIST);
			}
		});
		
		view_detail.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.VIEW_DETAIL);
			}
		});
		
		save_contact.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.SAVE_CONTACT);
			}
		});
		
		account.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.ACCOUNT);
			}
		});
		
		contribute_information.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.CONTRIBUTE_INFORMATION);
			}
		});
		
		update_data.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.UPDATE_DATA);
			}
		});
		
		back_to_main.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	onBackPressed();
			}
		});

		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.BACK);
			}
		});
		
		next.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.NEXT);
			}
		});
		
		back_to_menu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.BACK_TO_MENU);
			}
		});
		
		next_to_steps.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
            	controller.handleMessage(HelpController.NEXT_TO_STEPS);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		controller.setWhichPage(whichPage);
		controller.setWhichMenu(whichMenu);
		controller.setWhichStep(whichStep);
		
		Gate.initXGate(x2, x3);
		Gate.initGate(HelpActivity.this, isOpened);

		if (isOpened == false) {
			findViewById(R.id.loading).setVisibility(View.VISIBLE);
			Gate.openGate(this,(ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			setGateStatus(true);
		} else {
			if (whichPage == 1) {
				controller.initFirstPage();
				controller.setDetailGone();
				controller.setStepGone();
				controller.setMenuVisible();
			} else if (whichPage == 2) {
				controller.loadSecondPageData();
				controller.initSecondPage();
				controller.setMenuGone();
				controller.setStepGone();
				controller.setDetailVisible();
			} else {
				controller.setBackStepsFlipped();
				controller.loadThirdPageData();
				controller.initThirdPage();
				controller.setMenuGone();
				controller.setDetailGone();
				controller.setStepVisible();
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isBackPressed == false) {
			if (controller.getWhichPage() == 1) {
				Gate.crossfadeAkhir();
				Gate.initCloseGate(HelpActivity.this);
				Gate.getLastMove().setAnimationListener(new AnimationListener(){
		            public void onAnimationStart(Animation a){}
		            public void onAnimationRepeat(Animation a){}
		            public void onAnimationEnd(Animation a){
		            	HelpActivity.super.onBackPressed();
		            }
		        });

				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				isBackPressed = true;
			} else if (controller.getWhichPage() == 2){
				controller.handleMessage(HelpController.BACK_TO_MENU);
			} else {
				controller.handleMessage(HelpController.BACK);
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(GATE_STATUS, isOpened);
		savedInstanceState.putInt(PAGE_BANK, controller.getWhichPage());
		savedInstanceState.putInt(MENU_BANK, controller.getWhichMenu());
		savedInstanceState.putInt(STEP_BANK, controller.getWhichSteps());
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		setGateStatus(savedInstanceState.getBoolean(GATE_STATUS));
		whichPage = savedInstanceState.getInt(PAGE_BANK);
		whichMenu = savedInstanceState.getInt(MENU_BANK);
		whichStep = savedInstanceState.getInt(STEP_BANK);
	}
	
	private void setGateStatus(boolean is) {
		isOpened = is;
	}
}
