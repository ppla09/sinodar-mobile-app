package com.ppl.sinodar.controllers;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.widget.Toast;

import com.ppl.sinodar.SinodarApp;
import com.ppl.sinodar.activities.AboutUsActivity;
import com.ppl.sinodar.activities.BigMenuActivity;
import com.ppl.sinodar.activities.HelpActivity;
import com.ppl.sinodar.activities.LoginActivity;
import com.ppl.sinodar.activities.MainMenuActivity;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.activities.SubmitInstanceActivity;
import com.ppl.sinodar.activities.UpdateLocalCacheActivity;
import com.ppl.sinodar.daos.SessionDao;
import com.ppl.sinodar.helper.ConnectionHelper;
import com.ppl.sinodar.models.UserSessionModel;

public class MainMenuController extends Controller {

	/* code for message handling */
	public static final int MESSAGE_USER_LOGOUT = 1;
	public static final int MESSAGE_UPDATE_LOCAL_CACHE = 2;
	public static final int VIEW_LOGIN_FORM = 3;
	public static final int VIEW_BIG_MENU_HOSPITAL = 4;
	public static final int VIEW_BIG_MENU_FIRE_STATION = 5;
	public static final int VIEW_BIG_MENU_POLICE_STATION = 6;
	public static final int VIEW_NEW_ENTRY_FORM = 7;
	public static final int VIEW_HELP = 8;
	public static final int VIEW_ABOUT_US = 9;
	
	private MainMenuActivity view;
	private UserSessionModel model;

	public MainMenuController(MainMenuActivity view) {
		this.view = view;
		model = new UserSessionModel();
		model.addListener(this.view);

		workerThread = new HandlerThread("MainMenuController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());

		initiateSession();
	}

	public UserSessionModel getModel() {
		return model;
	}

	@Override
	public boolean handleMessage(int what, Object data) {
		switch (what) {
			case VIEW_LOGIN_FORM:
				viewLoginForm();
				return true;

			case MESSAGE_USER_LOGOUT:
				logout();
				return true;
				
			case VIEW_BIG_MENU_HOSPITAL:
				viewBigMenu(SinodarApp.HOSPITAL);
				return true;
				
			case VIEW_BIG_MENU_FIRE_STATION:
				viewBigMenu(SinodarApp.FIRE_STATION);
				return true;
				
			case VIEW_BIG_MENU_POLICE_STATION:
				viewBigMenu(SinodarApp.POLICE_STATION);
				return true;
				
			case VIEW_NEW_ENTRY_FORM:
				viewNewEntryForm();
				return true;
				
			case MESSAGE_UPDATE_LOCAL_CACHE:
				updateLocalCache();
				return true;
				
			case VIEW_HELP:
				viewHelp();
				return true;
				
			case VIEW_ABOUT_US:
				viewAboutUs();
				return true;
		}
		return false;
	}

	private void initiateSession() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (model) {
					UserSessionModel saved = SessionDao.loadUserSession(view);
					if (saved != null) model.consume(saved);
				}
			}
		});
	}

	private void viewLoginForm() {
		Intent intent = new Intent(view, LoginActivity.class);
		view.startActivity(intent);
	}

	private void viewBigMenu(int instanceCode) {
		Intent intent = new Intent(view, BigMenuActivity.class);
		intent.putExtra(BigMenuActivity.INSTANCE_CODE, instanceCode);
		view.startActivity(intent);
	}
	
	private void viewNewEntryForm() {
		if(!ConnectionHelper.isOnline()) {
			Toast.makeText(view.getApplicationContext(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
			return;
		}
		
		Intent intent = new Intent(view, SubmitInstanceActivity.class);
		intent.putExtra(SubmitInstanceActivity.USERNAME, model.getUserName());
		view.startActivity(intent);
	}

	private void updateLocalCache() {
		if(!ConnectionHelper.isOnline()) {
			Toast.makeText(view.getApplicationContext(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
			return;
		}
		
		view.startActivity(new Intent(view, UpdateLocalCacheActivity.class));
	}
	
	private void viewHelp() {
		view.startActivity(new Intent(view, HelpActivity.class));
	}
	
	private void viewAboutUs() {
		view.startActivity(new Intent(view, AboutUsActivity.class));
	}
	
	private void logout() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (model) {
					SessionDao.deleteUserSession(view);
					model.consume(new UserSessionModel());
				}

				Toast.makeText(view.getApplicationContext(), R.string.logout_success, Toast.LENGTH_SHORT).show();
			}
		});
	}
}
