package com.ppl.sinodar.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ppl.sinodar.animations.Gate;
import com.ppl.sinodar.controllers.MainMenuController;
import com.ppl.sinodar.models.OnChangeListener;
import com.ppl.sinodar.models.UserSessionModel;

public class MainMenuActivity extends ActionBarActivity implements OnChangeListener<UserSessionModel> {

	public static final String SHOW_TOAST_SUCCESS_SUBMIT = "toastSuccess";
	public static final String SHOW_TOAST_UNSTABLE_INTERNET_CACHE = "unstableIntCache";
	public static final String SHOW_TOAST_SUCCESS_CACHE = "cacheSuccess";
	
	private static final String LOGOUT_DIALOG_STATE = "logout";
	private static final String GATE_STATUS = "thisIsGate";

	private MainMenuController controller;

	private ImageButton hospitalBtn, fireBtn, policeBtn;
	private Dialog logoutDialog;

	private int x2, x3;
	private boolean isOpened, isBackPressed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
		if(getIntent().getBooleanExtra(SHOW_TOAST_SUCCESS_SUBMIT, false)) {
			Toast.makeText(this, R.string.success_to_submit, Toast.LENGTH_SHORT).show();
			getIntent().removeExtra(SHOW_TOAST_SUCCESS_SUBMIT);
		} else if(getIntent().getBooleanExtra(SHOW_TOAST_UNSTABLE_INTERNET_CACHE, false)) {
			Toast.makeText(this, R.string.unstable_internet_connection_cache, Toast.LENGTH_SHORT).show();
			getIntent().removeExtra(SHOW_TOAST_UNSTABLE_INTERNET_CACHE);
		} else if(getIntent().getBooleanExtra(SHOW_TOAST_SUCCESS_CACHE, false)) {
			Toast.makeText(this, R.string.success_to_update_cache, Toast.LENGTH_SHORT).show();
			getIntent().removeExtra(SHOW_TOAST_SUCCESS_CACHE);
		}

        int originalPos2[] = new int[2];
		findViewById(R.id.gate_right).getLocationOnScreen(originalPos2);
		x2 = originalPos2[1];
        
        int originalPos3[] = new int[2];
        findViewById(R.id.middle_loading).getLocationOnScreen(originalPos3);
        x3 = originalPos3[1];
        
        Gate.initXGate(x2, x3);
		
		controller = new MainMenuController(MainMenuActivity.this);

		hospitalBtn = (ImageButton) findViewById(R.id.hospital);
		fireBtn = (ImageButton) findViewById(R.id.fireStation);
		policeBtn = (ImageButton) findViewById(R.id.policStation);

		ActionBar actionBar = getSupportActionBar();
		actionBar.show();

		hospitalBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.instance_list_scroll_view).setVisibility(View.GONE);
				Gate.crossfadeAkhir();
				Gate.initCloseGate(MainMenuActivity.this);
				Gate.getLastMove().setAnimationListener(new AnimationListener(){
		            public void onAnimationStart(Animation a){}
		            public void onAnimationRepeat(Animation a){}
		            public void onAnimationEnd(Animation a){
		            	controller.handleMessage(MainMenuController.VIEW_BIG_MENU_HOSPITAL);
		            }
		        });
				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				setGateStatus(false);
			}
		});
		
		fireBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.instance_list_scroll_view).setVisibility(View.GONE);
				Gate.crossfadeAkhir();
				Gate.initCloseGate(MainMenuActivity.this);
				Gate.getLastMove().setAnimationListener(new AnimationListener(){
		            public void onAnimationStart(Animation a){}
		            public void onAnimationRepeat(Animation a){}
		            public void onAnimationEnd(Animation a){
		            	controller.handleMessage(MainMenuController.VIEW_BIG_MENU_FIRE_STATION);
		            }
		        });
				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				setGateStatus(false);
			}
		});

		policeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				findViewById(R.id.instance_list_scroll_view).setVisibility(View.GONE);
				Gate.crossfadeAkhir();
				Gate.initCloseGate(MainMenuActivity.this);
				Gate.getLastMove().setAnimationListener(new AnimationListener(){
		            public void onAnimationStart(Animation a){}
		            public void onAnimationRepeat(Animation a){}
		            public void onAnimationEnd(Animation a){
		            	controller.handleMessage(MainMenuController.VIEW_BIG_MENU_POLICE_STATION);
		            }
		        });
				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				setGateStatus(false);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		Gate.initXGate(x2, x3);
		Gate.initGate(MainMenuActivity.this, isOpened);
		if (isOpened == false) {
			Gate.openGate(this,(ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			setGateStatus(true);
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isBackPressed == false) {
			Gate.crossfadeAkhir();
			Gate.initCloseGate(MainMenuActivity.this);
			Gate.getLastMove().setAnimationListener(new AnimationListener(){
	            public void onAnimationStart(Animation a){}
	            public void onAnimationRepeat(Animation a){}
	            public void onAnimationEnd(Animation a){
	        		MainMenuActivity.super.onBackPressed();
	            }
	        });
			Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			isBackPressed = true;
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.dispose();
		if (logoutDialog != null && logoutDialog.isShowing()) logoutDialog.dismiss();
	}
	
	@Override
	public void onChange(UserSessionModel model) {
		supportInvalidateOptionsMenu();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		getMenuInflater().inflate(R.menu.main_menu, menu);

		UserSessionModel model = controller.getModel();

		MenuItem username = menu.findItem(R.id.actionbar_menu_username);
		MenuItem login = menu.findItem(R.id.actionbar_menu_login);
		MenuItem newEntry = menu.findItem(R.id.actionbar_menu_newEntry);
		MenuItem logout = menu.findItem(R.id.actionbar_menu_logout);
		MenuItem overflow = menu.findItem(R.id.overflow_menu);

		overflow.setVisible(true);
		boolean isLogged = model.getIsLogged();
		login.setVisible(!isLogged);
		username.setVisible(isLogged);
		newEntry.setVisible(isLogged);
		logout.setVisible(isLogged);

		if (isLogged) username.setTitle(model.getUserFirstName());

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.actionbar_menu_username:
				return true;
				
			case R.id.actionbar_menu_login:
            	controller.handleMessage(MainMenuController.VIEW_LOGIN_FORM);
				return true;
				
			case R.id.actionbar_menu_newEntry:
            	controller.handleMessage(MainMenuController.VIEW_NEW_ENTRY_FORM);
				return true;
				
			case R.id.actionbar_menu_updateData:
				controller.handleMessage(MainMenuController.MESSAGE_UPDATE_LOCAL_CACHE);
				return true;
				
			case R.id.actionbar_menu_help:
				controller.handleMessage(MainMenuController.VIEW_HELP);
				return true;
				
			case R.id.actionbar_menu_aboutUs:
				controller.handleMessage(MainMenuController.VIEW_ABOUT_US);
				return true;
				
			case R.id.actionbar_menu_logout:
				createLogoutDialog();
				return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(LOGOUT_DIALOG_STATE, (logoutDialog != null && logoutDialog.isShowing()));
		savedInstanceState.putBoolean(GATE_STATUS, isOpened);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.getBoolean(LOGOUT_DIALOG_STATE)) createLogoutDialog();
		if (savedInstanceState.getBoolean(GATE_STATUS)) setGateStatus(true); else setGateStatus(false);
	}

	private void createLogoutDialog() {
		if (logoutDialog != null && logoutDialog.isShowing()) logoutDialog.dismiss();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(getString(R.string.title_logout_confirmation));
		builder.setMessage(getString(R.string.logout_confirmation));
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				controller.handleMessage(MainMenuController.MESSAGE_USER_LOGOUT);
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				logoutDialog.dismiss();
			}
		});

		logoutDialog = builder.show();
	}
	
	private void setGateStatus(boolean is) {
		isOpened = is;
	}
}
