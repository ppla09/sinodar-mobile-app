package com.ppl.sinodar.lists;

import java.util.ArrayList;

import com.loopj.android.image.SmartImageView;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.models.ViewedInstanceModel;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class InstanceListAdapter extends ArrayAdapter<ViewedInstanceModel> {
 
	private LayoutInflater inflater;
	private Context context;
	
    public InstanceListAdapter(Context context, ArrayList<ViewedInstanceModel> data) {
    	super(context, R.layout.row_layout, data);
    	this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) convertView = inflater.inflate(R.layout.row_layout, parent, false);
 
        TextView name = (TextView) convertView.findViewById(R.id.row_name_label); 	 						// instance name
        TextView distance = (TextView) convertView.findViewById(R.id.row_distance_label); 					// relative distance
        SmartImageView thumb_image = (SmartImageView) convertView.findViewById(R.id.row_item_thumb);  		// thumb image
                
        ViewedInstanceModel instance = getItem(position);
        
        // Setting all values in listview
        name.setText(instance.getName());
        distance.setText(instance.getBeautifulDistance());
     
        if(instance.getPictureURL().length() > 0) thumb_image.setImageUrl(instance.getPictureURL()); 
        	else thumb_image.setImageBitmap(((BitmapDrawable) context.getResources().getDrawable(R.drawable.no_picture_available)).getBitmap());
                  	
        return convertView;
    }
}