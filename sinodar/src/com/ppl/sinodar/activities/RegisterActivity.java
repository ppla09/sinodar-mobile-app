package com.ppl.sinodar.activities;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ppl.sinodar.animations.Gate;
import com.ppl.sinodar.controllers.RegisterController;

public class RegisterActivity extends ActionBarActivity {

	private RegisterController controller;
	private static final String GATE_STATUS = "thisIsGate";
	
	private EditText userName, password, email, userFirstName, userLastName;
	private Button submitBtn;

	private int x2, x3;
	private boolean isOpened, isBackPressed;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		controller = new RegisterController(this);

        int originalPos2[] = new int[2];
		findViewById(R.id.gate_right).getLocationOnScreen(originalPos2);
		x2 = originalPos2[1];
        
        int originalPos3[] = new int[2];
        findViewById(R.id.middle_loading).getLocationOnScreen(originalPos3);
        x3 = originalPos3[1];
        
        Gate.initXGate(x2, x3);
		
		userName = (EditText) findViewById(R.id.register_username);
		password = (EditText) findViewById(R.id.register_password);
		email = (EditText) findViewById(R.id.register_email);
		userFirstName = (EditText) findViewById(R.id.register_userfirstname);
		userLastName = (EditText) findViewById(R.id.register_userlastname);
		submitBtn = (Button) findViewById(R.id.register_submit_btn);
		
		submitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				resetFieldError();
				ArrayList<String> field = new ArrayList<String>();
				field.add(userName.getText().toString());
				field.add(password.getText().toString());
				field.add(email.getText().toString());
				field.add(userFirstName.getText().toString());
				field.add(userLastName.getText().toString());
				
				controller.handleMessage(RegisterController.MESSAGE_USER_REGISTER, field);
			}
		});
		
		ActionBar actionBar = getSupportActionBar();
		actionBar.show();
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		Gate.initXGate(x2, x3);
		Gate.initGate(RegisterActivity.this, isOpened);
		if (isOpened == false) {
			findViewById(R.id.loading).setVisibility(View.GONE);
			findViewById(R.id.loading2).setVisibility(View.VISIBLE);
			Gate.changeLoadScreen(findViewById(R.id.loading2));
			Gate.openGate(this,(ImageView)findViewById(R.id.gate_left2),(ImageView)findViewById(R.id.gate_right2),(RelativeLayout)findViewById(R.id.middle_loading2));
			setGateStatus(true);
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isBackPressed == false) {
			findViewById(R.id.loading).setVisibility(View.GONE);
			findViewById(R.id.loading2).setVisibility(View.VISIBLE);
			Gate.changeLoadScreen(findViewById(R.id.loading2));
			Gate.crossfadeAkhir();
			Gate.initCloseGate(this);
			Gate.getLastMove().setAnimationListener(new AnimationListener(){
	            public void onAnimationStart(Animation a){}
	            public void onAnimationRepeat(Animation a){}
	            public void onAnimationEnd(Animation a){
	        		RegisterActivity.super.onBackPressed();
	            }
	        });
			Gate.startCloseGate((ImageView)findViewById(R.id.gate_left2),(ImageView)findViewById(R.id.gate_right2),(RelativeLayout)findViewById(R.id.middle_loading2));
			isBackPressed = true;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		resetFieldError();
		controller.dispose();
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean(GATE_STATUS, isOpened);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.getBoolean(GATE_STATUS)) {
			setGateStatus(true);
		} else {
			setGateStatus(false);
		}
	}


	public void resetFieldError() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				userName.setError(null);
				password.setError(null);
				email.setError(null);
				userFirstName.setError(null);
			}
		});
	}
	
	public void setUserNameFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				userName.setError(getString(errorId));
				userName.requestFocus();
			}
		});
	}
	
	public void setPasswordFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				password.setError(getString(errorId));
				password.requestFocus();
			}
		});
	}
	
	public void setEmailFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				email.setError(getString(errorId));
				email.requestFocus();
			}
		});
	}
	
	public void setUserFirstNameFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				userFirstName.setError(getString(errorId));
				userFirstName.requestFocus();
			}
		});
	}
	
	public void setUserLastNameFieldError(final int errorId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				userLastName.setError(getString(errorId));
				userLastName.requestFocus();
			}
		});
	}
	
	public void onLoading() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.loading2).setVisibility(View.GONE);
				findViewById(R.id.loading).setVisibility(View.VISIBLE);
				changeAlphaLoading(findViewById(R.id.logging_in), 0.0f, 1.0f);
				findViewById(R.id.list_progress_bar).setVisibility(View.VISIBLE);
				Gate.changeLoadScreen(findViewById(R.id.loading));
				Gate.crossfadeAkhir();
				Gate.initCloseGate(RegisterActivity.this);
				Gate.startCloseGate((ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
			}
		});
	}
	
	public void onRestoreForm() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.loading2).setVisibility(View.GONE);
				findViewById(R.id.loading).setVisibility(View.VISIBLE);
				changeAlphaLoading(findViewById(R.id.logging_in), 1.0f, 0.0f);
				findViewById(R.id.list_progress_bar).setVisibility(View.GONE);
				Gate.initXGate(x2, x3);
				Gate.initGate(RegisterActivity.this, isOpened);
				Gate.changeLoadScreen(findViewById(R.id.loading));
				Gate.openGate(RegisterActivity.this,(ImageView)findViewById(R.id.gate_left),(ImageView)findViewById(R.id.gate_right),(RelativeLayout)findViewById(R.id.middle_loading));
				setGateStatus(true);
			}
		});
	}

	private void setGateStatus(boolean is) {
		isOpened = is;
	}
	
	public static void changeAlphaLoading(View v, float from, float to) {
    	AlphaAnimation animateAlphaChange = new AlphaAnimation(from,to);
    	animateAlphaChange.setDuration(0);
    	animateAlphaChange.setFillAfter(true);
    	v.startAnimation(animateAlphaChange);
    }
}
