package com.ppl.sinodar.controllers;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.Toast;

import com.ppl.sinodar.activities.LocationPickerActivity;
import com.ppl.sinodar.activities.MainMenuActivity;
import com.ppl.sinodar.activities.R;
import com.ppl.sinodar.activities.SubmitInstanceActivity;
import com.ppl.sinodar.daos.InstanceDao;
import com.ppl.sinodar.helper.ImageHelper;
import com.ppl.sinodar.helper.LocationHelper;

public class SubmitInstanceController extends Controller {

	/* code for handling message */
	public static final int MESSAGE_CHOOSE_PICTURE = 1;
	public static final int MESSAGE_HANDLE_PICTURE = 2;
	public static final int MESSAGE_SET_INSTANCE_LOCATION = 3;
	public static final int MESSAGE_HANDLE_LOCATION_PICKED = 4;
	public static final int MESSAGE_SUBMIT_DATA = 5;
	
	private SubmitInstanceActivity view;
	private String username;
	private double chosenLatitude, chosenLongitude;
	private Uri chosenImageUri = null; 
	private boolean hasLocationPicked = false;
	
	public SubmitInstanceController(SubmitInstanceActivity view, String username) {
		this.view = view;
		this.username = username;

		workerThread = new HandlerThread("SubmitInstanceController Worker Thread");
		workerThread.start();
		workerHandler = new Handler(workerThread.getLooper());
		
		setInitialMap();
	}
	
	public Uri getChosenImageUri() { return chosenImageUri; }
	public boolean getHasInstanceLocationPicked() { return hasLocationPicked; }
	public double getChosenInstanceLatitude() { return chosenLatitude; }
	public double getChosenInstanceLongitude() { return chosenLongitude; }
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean handleMessage(int what, Object data) {
		switch(what) {
			case MESSAGE_CHOOSE_PICTURE:
				chooseInstancePicture();
				return true;
				
			case MESSAGE_HANDLE_PICTURE:
				handleChosenPicture((Uri) data);
				return true;
				
			case MESSAGE_SET_INSTANCE_LOCATION:
				setInstanceLocation();
				return true;
				
			case MESSAGE_HANDLE_LOCATION_PICKED:
				handleLocationPicked((ArrayList<Double>) data);
				return true;
				
			case MESSAGE_SUBMIT_DATA:
				submitNewInstanceData((ArrayList<Object>) data);
				return true;
		}
		return false;
	}
	
	private void chooseInstancePicture() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				Intent selectPhoto = new Intent(Intent.ACTION_GET_CONTENT);
				selectPhoto.setType("image/*");
				
				Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

				Intent chooserIntent = Intent.createChooser(selectPhoto, "Select or take a new picture");
				chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {takePhoto});

				view.startActivityForResult(chooserIntent, MESSAGE_CHOOSE_PICTURE);
			}
		});
	}
	
	private void handleChosenPicture(final Uri imageUri) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				chosenImageUri = imageUri;
				view.setInstancePicture(ImageHelper.getCorrectlyOrientedImage(view, imageUri));
			}
		});
	}
	
	private void setInstanceLocation() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(view, LocationPickerActivity.class);
				
				// put old marker (if any)
				synchronized((Boolean) hasLocationPicked) {
					if(hasLocationPicked) {
						intent.putExtra(LocationPickerActivity.LATITUDE, chosenLatitude);
						intent.putExtra(LocationPickerActivity.LONGITUDE, chosenLongitude);
					}
				}

				view.startActivityForResult(intent, MESSAGE_SET_INSTANCE_LOCATION);
			}
		});
	}
	
	private void handleLocationPicked(final ArrayList<Double> locationPicked) {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				synchronized((Boolean) hasLocationPicked) {
					hasLocationPicked = true;
					chosenLatitude  = locationPicked.get(0);	// by convention: latitude  = idx 0
					chosenLongitude = locationPicked.get(1);	//				  longitude = idx 1
					
					loadMap(chosenLatitude, chosenLongitude, "red");
				}
			}
		});
	}
	
	private boolean validateField(String type, String name, String addr, ArrayList<String> phoneNum, int chosenMain, String desc) {
		boolean valid = true;
		
		/* field error */
		if(desc.length() > 1500) {
			view.setInstanceDescFieldError(R.string.error_field_too_long_1500);
			valid = false;
		}
		
		if(chosenMain != -1 && phoneNum.get(chosenMain).length() == 0) {
			view.setPhoneNumberFieldError(chosenMain, R.string.error_field_required);
			valid = false;
		} else for(int i = 0; i < phoneNum.size(); i++) if(phoneNum.get(i).length() > 20) {
			view.setPhoneNumberFieldError(i, R.string.error_field_too_long_20);
			valid = false;
		} else {
			boolean unique = true;
			for(int j = 1; j < phoneNum.size() && unique; j++) if(!phoneNum.get(j).isEmpty()) {
				for(int k = j - 1; k >= 0 && unique; k--) unique = !(phoneNum.get(j).equals(phoneNum.get(k)));
				
				if(!unique) {
					view.setPhoneNumberFieldError(i, R.string.error_phonenum_not_unique);
					valid = false;
				}
			}
		}
		
		
		if(addr.length() > 200) {
			view.setInstanceAddrFieldError(R.string.error_field_too_long_200);
			valid = false;
		}
		
		if(TextUtils.isEmpty(name)) {
			view.setInstanceNameFieldError(R.string.error_field_required);
			valid = false;
		} else if(name.length() > 200) {
			view.setInstanceNameFieldError(R.string.error_field_too_long_200);
			valid = false;
		}
		
		/* Toast error */
		if(valid && chosenMain == -1) {
			Toast.makeText(view, R.string.main_phone_not_chosen, Toast.LENGTH_SHORT).show();
			valid = false;
		}
		
		if(valid && !hasLocationPicked) {
			Toast.makeText(view, R.string.location_not_chosen, Toast.LENGTH_SHORT).show();
			valid = false;
		}
		
		if(valid && !type.equals("hospital") && !type.equals("fire station") && !type.equals("police station")) {
			Toast.makeText(view, R.string.instance_type_not_chosen, Toast.LENGTH_SHORT).show();
			valid = false;
		}
		
		return valid;
	}
	
	private void submitNewInstanceData(final ArrayList<Object> data) {
		workerHandler.post(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				String type = null;												// by convention: type = idx 0
				switch((String) data.get(0)) {									//				  name = idx 1
					case("Hospital") : type = "hospital"; break;				//				  addr = idx 2
					case("Fire Station") : type = "fire station"; break;		//				  phone = idx 3
					case("Police Station") : type = "police station"; break;	//				  chosenMain = idx 4
				}																//				  desc = idx 5
				
				String name = (String) data.get(1);
				String addr = (String) data.get(2);
				ArrayList<String> phoneNum = (ArrayList<String>) data.get(3);
				int chosenMain = (Integer) data.get(4);
				String desc = (String) data.get(5);
				
				if(!validateField(type, name, addr, phoneNum, chosenMain, desc)) return;
				
				view.onLoading();
				boolean success;
				
				try { 
					success = InstanceDao.postSubmitInstance(view, username, type, name, addr, desc, chosenLatitude, chosenLongitude, chosenMain, phoneNum, chosenImageUri);
				} catch (Exception e) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.unstable_internet_connection_submit, Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(!success) {
					view.onRestoreForm();
					Toast.makeText(view, R.string.failed_to_submit, Toast.LENGTH_SHORT).show();
				} else {
					Intent intent = new Intent(view, MainMenuActivity.class);
					intent.putExtra(MainMenuActivity.SHOW_TOAST_SUCCESS_SUBMIT, true);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					view.startActivity(intent);
				}
			}
		});
	}

	private void setInitialMap() {
		workerHandler.post(new Runnable() {
			@Override
			public void run() {
				Location location = LocationHelper.getCurrentLoc(view, 500);
				double latitude, longitude;
				
				if(location != null) {
					latitude = location.getLatitude();
					longitude = location.getLongitude();
				} else {
					latitude = -6.357565;	// TODO: ganti!
			    	longitude = 106.832388;	// TODO: ganti!
				}
				
				loadMap(latitude, longitude, "blue");
			}
		});
	}
	
	private void loadMap(double latitude, double longitude, String color) {
		int size = ImageHelper.getDeviceWidth(view);
		String url = "http://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude
				+ "&zoom=15&size=" + size + "x" + size + "&markers=color:" + color + "%7C" + latitude + "," + longitude  
				+ "&sensor=false&key=AIzaSyBieC9-5mcClPiCC_zd9cRFDJR-wc7R07I";
		
        view.onLoadingInstanceMap();
        Bitmap image = ImageHelper.getImage(url);
        view.onLoadedInstanceMap(image);
	}
}
