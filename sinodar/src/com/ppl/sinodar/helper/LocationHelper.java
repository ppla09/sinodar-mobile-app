package com.ppl.sinodar.helper;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

public class LocationHelper {
	
	/* TODO: ganti jadi pakai current location, bukan last known. Pakai GPS, jangan network */
	public static Location getCurrentLoc(Activity act, int timeout) {
		LocationManager locationManager = (LocationManager) act.getSystemService(Context.LOCATION_SERVICE);
	    Location now_loc = null;
	    
	    int counter = 0;
		while(now_loc == null || counter < timeout){
			now_loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if(now_loc == null) now_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			counter++;
		}
		
		return now_loc;
	}
	
}
