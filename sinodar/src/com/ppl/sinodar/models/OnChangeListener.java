package com.ppl.sinodar.models;

public interface OnChangeListener<T> {
	
	void onChange(final T model);
	
}
