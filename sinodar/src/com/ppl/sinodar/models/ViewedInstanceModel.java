package com.ppl.sinodar.models;

import java.util.ArrayList;

/* TODO: change to Parcelable for faster operation speed */
public class ViewedInstanceModel extends InstanceModel {
	
	private static final long serialVersionUID = 1L;
	
	private double distance = -1;	/* TODO get rid of this, change to actual distance calculation */
	private String pictureURL = "";
	private String address = "";
	private String description = "";
	private ArrayList<String> otherPhoneNumber = new ArrayList<String>();
		
	public double getDistance() { return distance; }
	public String getPictureURL() { return pictureURL; }
	public String getAddress() { return address; }
	public String getDescription() { return description; }
	public ArrayList<String> getOtherPhoneNumber() { return otherPhoneNumber; }
	
	public String getBeautifulDistance() {
		String ret = "";
		if(distance >= 1) {
			double formatted = ((int)(distance * 100)) / 100.0; 
			ret = Double.toString(formatted) + " km";
		} else ret = Integer.toString((int) (distance * 1000.0)) + " m";
		
		return ret;
	}
	
	public void setDistance(double distance) {
		this.distance = distance;
		notifyObservers(this);
	}
	
	public void setPictureURL(String pictureURL) {
		this.pictureURL = pictureURL;
		notifyObservers(this);
	}

	public void setAddress(String address) {
		this.address = address;
		notifyObservers(this);
	}

	public void setDescription(String description) {
		this.description = description;
		notifyObservers(this);
	}

	public void setOtherPhoneNumber(ArrayList<String> otherPhoneNumber) {
		this.otherPhoneNumber = otherPhoneNumber;
		notifyObservers(this);
	}

	@Override
	synchronized public ViewedInstanceModel clone() {
		ViewedInstanceModel model = new ViewedInstanceModel();
		model.setId(id);
		model.setName(name);
		model.setInstanceType(instanceType);
		model.setLatitude(latitude);
		model.setLongitude(longitude);
		model.setMainPhoneNumber(mainPhoneNumber);
		model.setDistance(distance);
		model.setPictureURL(pictureURL);
		model.setAddress(address);
		model.setDescription(description);
		model.setOtherPhoneNumber(otherPhoneNumber);
		return model;
	}
	
	synchronized public void consume(ViewedInstanceModel model) {
		this.distance = model.getDistance();
		this.pictureURL = model.getPictureURL();
		this.address = model.getAddress();
		this.description = model.getDescription();
		this.otherPhoneNumber = model.getOtherPhoneNumber();
		super.consume(model);
	}
}
